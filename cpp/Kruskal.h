#ifndef KRUSKAL_H_
#define KRUSKAL_H_
#include <vector>
#include <queue>
#include <iostream>
#include "minitree.h"
using namespace std;


/*
 * Retrieves all leaves of tree defined by <treeRoot,treeEdges,leafN> into the vector leaves
 */
void getLeaves(TVertex treeRoot,
	       vector<Edge> const& treeEdges,
	       TVertex leafN,
	       vector<TVertex>& leaves);

/*
 * Performes single link clustering using Kruskal's algorithm.
 * edgesIn          vector of input edges, this will be sorted
 * leaf             number of leaf vertices
 * r                report after processing every r:th input edge, r==0 stands for no reporting (default)
 * minimum_tree     if true, returns minimum spanning trees, otherwise maximum spanning trees
 * treeEdges        vector of edges specifying cluster trees, to see how to use this see printTree(..)
 * treeRoots        a set of unique root nodes to be used as entry points in tree traversal
 * singletons       vector of leaf vertices that are left as singletons
 */
void kruskal(std::vector<Edge>& edgesIn,
	     TVertex leafN,
	     TVertex r,
	     bool minimum_tree,
	     std::vector<Edge>& treeEdges,
	     std::vector<TVertex>& treeRoots,
	     std::vector<TVertex>& singletons,
	     bool DEBUG);


// IMPLEMENTATION
bool cargoLessThen(Edge const& a1, Edge const& a2){
    return a1.cargo < a2.cargo;
}
bool cargoLargerThen(Edge const& a1, Edge const& a2){
    return a1.cargo > a2.cargo;
}


inline void updateVertexRoots(vector<Edge> const& treeEdges, 
			      TVertex leafN,
			      TVertex oldRoot,
			      vector<TVertex>& vertexRoots,
			      TVertex newRoot){
  queue<TVertex> vqueue;
  vqueue.push(oldRoot);
  TVertex v;
  while(!vqueue.empty()){
    v= vqueue.front();vqueue.pop();
    vertexRoots[v]= newRoot;
    if(v>=leafN){
      vqueue.push(treeEdges[v-leafN].source);
      vqueue.push(treeEdges[v-leafN].target);
    }
  }
}

void getLeaves(TVertex treeRoot,
	       vector<Edge> const& treeEdges,
	       TVertex leafN,
	       vector<TVertex>& leaves){
  leaves.clear();
  queue<TVertex> vqueue;
  vqueue.push(treeRoot);
  TVertex v;
  while(!vqueue.empty()){
    v= vqueue.front();vqueue.pop();
    if(v>=leafN){
      vqueue.push(treeEdges[v-leafN].source);
      vqueue.push(treeEdges[v-leafN].target);
    }
    else{
      leaves.push_back(v);
    }
  }
}

void kruskal(std::vector<Edge>& edgesIn,
	     TVertex leafN,
	     TVertex r,
	     bool minimum_tree,
	     std::vector<Edge>& treeEdges,
	     std::vector<TVertex>& treeRoots,
	     std::vector<TVertex>& singletons,
	     bool DEBUG){
  if(DEBUG){
    std::cout << "running kruskal()..\n";
  }
  if(minimum_tree){
    std::sort(edgesIn.begin(), edgesIn.end(),cargoLessThen);
  }
  else{
    std::sort(edgesIn.begin(), edgesIn.end(),cargoLargerThen);
  }
  treeEdges.clear();
  treeRoots.clear();
  vector<TVertex> vertexRoots(leafN*2,0);
  TVertex i=0;
  for(vector<TVertex>::iterator it=vertexRoots.begin(); it<vertexRoots.end(); it++,i++)
    *it= i;

  Edge edge;
  TVertex source;
  TVertex target;
  TVertex sourceRoot;
  TVertex targetRoot;
  TVertex newVertex;
  TVertex edgesN= edgesIn.size();
  for(TVertex edgei=0; edgei<edgesN; edgei++){
    edge= edgesIn[edgei];
    source= edge.source;
    target= edge.target;
    sourceRoot= vertexRoots[source];
    targetRoot= vertexRoots[target];
    
    if(sourceRoot != targetRoot){
      newVertex= treeEdges.size()+leafN;
      treeEdges.push_back(Edge(sourceRoot,targetRoot,edge.cargo));
      vertexRoots[newVertex]= newVertex;
      updateVertexRoots(treeEdges,leafN,sourceRoot,vertexRoots,newVertex);
      updateVertexRoots(treeEdges,leafN,targetRoot,vertexRoots,newVertex);
      /*DEBUG
      cout<< "adding input edge=(i="<<edgei<<","<<edge<<") as tree edge=(i"<<newVertex<<","<<(treeEdges.back())<<")\n";
      cout<< "vertex roots: ";
      for(unsigned int i=0; i<vertexRoots.size(); i++)
	cout << " "<<vertexRoots[i];
      cout<<"\n";
      */
    }
    if(r>0 && edgei>0 && edgei%r==0){
      cout<< "kruskal(): "<<edgei<< " edges processed\n";
      cout.flush();
    }
  }

  // Retrieving the list of distinctive roots
  treeRoots.clear();
  TVertex edgei,vertexi;
  for(edgei=0,vertexi=leafN; edgei< treeEdges.size(); edgei++,vertexi++){
    if(vertexi==vertexRoots[vertexi])
      treeRoots.push_back(vertexi);
  }
  // singletons
  singletons.clear();
  for(vertexi=0; vertexi<leafN; vertexi++){
    if(vertexRoots[vertexi]==vertexi)
      singletons.push_back(vertexi);
  }

  //DEBUG
  /*cout << "destinctive roots:\n";
  for(unsigned int i=0; i<treeRoots.size(); i++){
    cout << treeRoots[i]<<"\n";
    }
  */
}


#endif /*KRUSKAL_H_*/

