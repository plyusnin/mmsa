#ifndef CLIQUETRANSFORMATION_H
#define CLIQUETRANSFORMATION_H
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <seqan/basic.h>
#include <seqan/graph_msa.h>
#include "Kruskal.h"
using namespace seqan;



template<typename TStringSet, 
	 typename TCargo, 
	 typename TSpec>
inline void
cliqueTransformation_simple(Graph<Alignment<TStringSet, TCargo, TSpec> >& g, bool DEBUG=false){

  typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
  typedef typename Size<TGraph>::Type TSize;
  typedef typename VertexDescriptor<TGraph>::Type TVertexDescriptor;
  typedef typename Iterator<TGraph, VertexIterator>::Type TVertexIterator;
  typedef typename EdgeDescriptor<TGraph>::Type TEdgeDescriptor; 
  typedef typename Iterator<TGraph, EdgeIterator>::Type TEdgeIterator;
  typedef String<Edge> TEdgeString;
  
  // map vertexDescriptors to TVertex for usage of Kruskals code
  if(DEBUG){
    std::cout << "cliqueTransformation_simple: creating index2vertex & vertex2index maps..\n";
    std::cout.flush();
  }
  TSize edgeN= numEdges(g);
  TSize leafN= numVertices(g);
  std::vector<TVertexDescriptor> index2vertex(leafN,0);
  std::map<TVertex,TVertexDescriptor> vertex2index;
  std::vector<Edge> edges(edgeN,Edge(0,0,0));
  std::vector<Edge> treeEdges;
  std::vector<TVertex> treeRoots;
  std::vector<TVertex> singletons;
  std::vector<TVertex> leavesLeft;
  std::vector<TVertex> leavesRight;

  TVertex i=0;
  TVertexIterator itV(g);
  for(;!atEnd(itV);goNext(itV),i++){
    vertex2index[*itV]= i;
    index2vertex[i]= *itV;
  }

  // convert seqan edges to edges used by Kruskal.h
  if(DEBUG){
    std::cout << "cliqueTransformation_simple: converting AlignmentGraph edges to Edge-vector..\n";
    std::cout.flush();
  }
  i=0;
  TEdgeIterator itE(g);
  for(;!atEnd(itE);goNext(itE),i++) {
    TVertexDescriptor sV = sourceVertex(itE);
    TVertexDescriptor tV = targetVertex(itE);
    TCargo c = cargo(*itE);
    edges[i].source= vertex2index[sV];
    edges[i].target= vertex2index[tV];
    edges[i].cargo = c;
  }


  // run Kruskal's
  kruskal(edges,
	  leafN,
	  0,
	  false,
	  treeEdges,
	  treeRoots,
	  singletons,
	  DEBUG);
  /* DEBUG
  cout << "#DEBUG: kruskal trees:\n";
  cout << "#seqi\tvertex\tindex:\n";
  for(TVertex i=0; i<leafN; i++){
    cout <<sequenceId(g,i)<<"\t"<< label(g,index2vertex[i])<<"\t"<<i<<"\n";
  }
  cout << "#tree roots:\n";
  for(unsigned int i=0; i<treeRoots.size(); i++)
    cout<<" "<<treeRoots[i];
  cout<<"\n";
  for(unsigned int i=0; i<treeRoots.size(); i++){
    cout << "#tree "<<i<<"\n";
    printTree(treeRoots[i],treeEdges,leafN);
  }
  */

  // construct new Alignment Graph
  if(DEBUG){
    std::cout << "cliqueTransformation_simple: adding edges between subtree leaves..\n";
    std::cout.flush();
  }
  clearEdges(g);
  for(std::vector<TVertex>::iterator it= treeRoots.begin(); it< treeRoots.end(); it++){
    std::queue<TVertex> vqueue;
    vqueue.push(*it);
    TVertex v,s,t;
    while(!vqueue.empty()){
      v= vqueue.front();vqueue.pop();
      if(v>=leafN){
	s= treeEdges[v-leafN].source;
	t= treeEdges[v-leafN].target;
	getLeaves(s,treeEdges,leafN,leavesLeft);
	getLeaves(t,treeEdges,leafN,leavesRight);
	for(TVertex i=0; i<leavesLeft.size(); i++){
	  for(TVertex j=0; j<leavesRight.size(); j++){
	    addEdge(g, index2vertex[leavesLeft[i]],index2vertex[leavesRight[j]],treeEdges[v-leafN].cargo);	    
	  }
	}
	vqueue.push(s);
	vqueue.push(t);
      }
    }
  }
  if(DEBUG){
    std::cout << "cliqueTransformation_simple: done\n";
    std::cout.flush();
  }
}




#endif
