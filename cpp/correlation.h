#ifndef CORRELATION_H
#define CORRELATION_H
#include <vector>
#include <math.h>
using namespace std;

/*
 * Calculates and returns Spearman Correlation coefficient between two vectors.
 */
inline
double getSpearmanCorrelation(const vector<double>& x,const vector<double>& y){
	if(x.empty() || y.empty()){
		return 0.;
	}

	double n= (x.size()<y.size())?x.size():y.size();
	double sx=0;
	double sy=0;
	for(unsigned int i=0; i<n; i++){
		sx+= x[i];
		sy+= y[i];
	}
	double mx=sx/n;
	double my=sy/n;

	double cov=0;
	double varx=0;
	double vary=0;
	for(unsigned int i=0; i<n; i++){
		cov+= (x[i]-mx)*(y[i]-my);
		varx+= (x[i]-mx)*(x[i]-mx);
		vary+= (y[i]-my)*(y[i]-my);
	}
	return cov/(sqrt(varx*vary));
}

#endif

