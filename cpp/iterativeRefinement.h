#ifndef ITERATIVE_REFINEMENT_H
#define ITERATIVE_REFINEMENT_H
#include <seqan/graph_types.h>
#include <seqan/graph_msa/graph_align_tcoffee_progressive.h>
using namespace seqan;


/*
 * Scores profile by calculating the sum of edges that agree with profile structure.
 */
template<typename TStringSet, typename TCargo, typename TSpec, typename TSegmentString>
inline TCargo
profileSumOfEdgeWeights(Graph<Alignment<TStringSet, TCargo, TSpec> > const& g,
						TSegmentString const& profile){
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Size<TGraph>::Type TSize;
	typedef typename VertexDescriptor<TGraph>::Type TVertexDescriptor;
	typedef typename Value<TSegmentString>::Type TVertexSet;

	// Remember for each vertex descriptor the position in the profile
	typedef String<TSize> TMapVertexPos;
	TMapVertexPos map;
	resize(map,getIdUpperBound(_getVertexIdManager(g)),MaxValue<TSize>::VALUE);
	typedef typename Iterator<TSegmentString const, Standard>::Type TSegmentStringIterConst;
	typedef typename Iterator<TVertexSet const, Standard>::Type TVertexSetIterConst;
	TSegmentStringIterConst itP = begin(profile, Standard());
	TSegmentStringIterConst itPEnd = end(profile, Standard());
	TSize pos = 0;
	TVertexSetIterConst itV;
	TVertexSetIterConst itVEnd;
	for(;itP != itPEnd; ++itP, ++pos) {
		itV = begin(*itP, Standard());
		itVEnd = end(*itP, Standard());
		for(;itV != itVEnd;++itV) map[*itV] = pos;
	}

	// only edges that join vertices in the same profile position contribute to the score
	// DEBUG
	//cout << "scoring profile:\n";
	TCargo score=0;
	typedef typename Iterator<TGraph, EdgeIterator>::Type TEdgeIterator;
    TEdgeIterator itE(g);
    for(;!atEnd(itE);goNext(itE)) {
        if(map[sourceVertex(itE)] == map[targetVertex(itE)]){
			score+= (TCargo) cargo(*itE);
			/*DEBUG
			cout << sequenceId(g,sourceVertex(itE))<<":"<<label(g,sourceVertex(itE))<<" - "
				<< sequenceId(g,targetVertex(itE))<<":"<<label(g,targetVertex(itE))
				<< " "<< cargo(*itE)
				<<"\n";*/
		}
    }
    return score;
}

/*
 * Iteratively refines root profile by randomly partitioning sequences into two groups.
 * For each of the groups a profile is constructed, referred as upper and lower profiles,
 * which are then realignd. If realigning improves sum of pairs score, the new alignment
 * is partitioned and iteratively realigned further.
 */
template<typename TStringSet, typename TCargo, typename TSpec, typename TSegmentString>
inline void
iterativeRefinement_random(Graph<Alignment<TStringSet, TCargo, TSpec> > const& g,
						TSegmentString& rootProfile,
						int irefine){
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Size<TGraph>::Type TSize;
	TStringSet seqSet= stringSet(g);
	TSize nLeaves= length(seqSet);
	TSegmentString upperProfile;
	TSegmentString lowerProfile;
	TSegmentString tempProfile;
	TCargo sc= profileSumOfEdgeWeights(g,rootProfile);
	TCargo bsc=sc;
	unsigned int seed=1;
	srand(seed);
	//DEBUG
		//printf("score=%.2f\n",sc);
	std::vector<bool> partition(nLeaves,false);
	for(unsigned int ir=0; ir<irefine; ir++){
		// create partition
		for(unsigned int j=0; j<partition.size(); j++)
			partition[j]= rand() < RAND_MAX/2;
		clear(upperProfile); // clearing vertices from profiles and reserving memo for position strings
		clear(lowerProfile);
		resize(upperProfile,length(rootProfile));
		resize(lowerProfile,length(rootProfile));

		for(unsigned int i=0; i<length(rootProfile); i++){
			for(unsigned int j=0; j<length(rootProfile[i]); j++){
				TSize seqPos= idToPosition(seqSet,sequenceId(g,rootProfile[i][j]));
				if(partition[seqPos])
					append(upperProfile[i],rootProfile[i][j]);
				else
					append(lowerProfile[i],rootProfile[i][j]);
			}
		}

		// pruning empty segments
		TSegmentString _upperProfile;
		TSegmentString _lowerProfile;
		for(unsigned int i=0; i<length(rootProfile); i++){
			if(length(upperProfile[i])>0)
				appendValue(_upperProfile,upperProfile[i]);
			if(length(lowerProfile[i])>0)
				appendValue(_lowerProfile,lowerProfile[i]);
		}
		upperProfile= _upperProfile;
		lowerProfile= _lowerProfile;

		// realign
		heaviestCommonSubsequence(g, upperProfile, lowerProfile, tempProfile);
		sc= profileSumOfEdgeWeights(g,tempProfile);
		/*DEBUG
			printf("ir=%u sc=%2.f partition=[",ir,sc);
			for(unsigned int j=0; j<partition.size(); j++) if(partition[j]) printf("%u,",j);
			printf("] [");
			for(unsigned int j=0; j<partition.size(); j++) if(!partition[j]) printf("%u,",j);
			printf("]\n");*/

		if(sc>bsc){
			bsc= sc;
			rootProfile= tempProfile;
		}
	}
}

/*
 * Partition seqs into two profiles by cutting a random edge of the guiding tree.
 */
template<typename TStringSet, typename TCargo, typename TSpec, typename TGuideTree, typename TSegmentString>
inline void
iterativeRefinement_tree1(Graph<Alignment<TStringSet, TCargo, TSpec> > const& g,
						TGuideTree& guideTree,
						TSegmentString& rootProfile,
						int irefine){
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Size<TGraph>::Type TSize;
	TStringSet seqSet= stringSet(g);
	TSize nLeaves= length(seqSet);
	TSegmentString upperProfile;
	TSegmentString lowerProfile;
	TSegmentString tempProfile;
	TCargo sc= profileSumOfEdgeWeights(g,rootProfile);
	TCargo bsc=sc;
	unsigned int seed=1;
	srand(seed);
	//DEBUG
		//printf("score=%.2f\n",sc);
	std::vector<bool> partition(nLeaves,false);


	// collect guiding tree nodes
	typedef typename VertexDescriptor<TGuideTree>::Type TVertexDescriptor;
	typedef String<TVertexDescriptor> TVertexString;
	typedef typename Iterator<TGuideTree, BfsIterator>::Type TBfsIterator;
	TSize nVertices = numVertices(guideTree)-1;
	TVertexString vertices;
	resize(vertices, nVertices);
	TVertexDescriptor rootVertex = getRoot(guideTree);
	TBfsIterator bfsIt(guideTree, rootVertex);
	goNext(bfsIt); // root excluded
	for(TSize i=0; !atEnd(bfsIt); goNext(bfsIt),i++){
		vertices[i]= *bfsIt;
	}
	/* DEBUG
	cout<<"# DEBUG guiding tree nodes in bfs order (no root, leaves marked with *)\n";
	for(unsigned int i=0; i<nVertices; i++){
		if(isLeaf(guideTree,vertices[i]))
			cout <<vertices[i]<<"*\n";
		else
			cout <<vertices[i]<<"\n";
	}*/



	for(unsigned int ir=0; ir<irefine; ir++){
		// create partition
		unsigned int r= rand()%nVertices;
		String<TSize> group;
		collectLeaves(guideTree, vertices[r], group);
		partition.assign(nLeaves,false);
		for(unsigned int i=0; i<length(group); i++)
			partition[group[i]]= true;

		clear(upperProfile); // clearing vertices from profiles and reserving memo for position strings
		clear(lowerProfile);
		resize(upperProfile,length(rootProfile));
		resize(lowerProfile,length(rootProfile));

		for(unsigned int i=0; i<length(rootProfile); i++){
			for(unsigned int j=0; j<length(rootProfile[i]); j++){
				TSize seqPos= idToPosition(seqSet,sequenceId(g,rootProfile[i][j]));
				if(partition[seqPos])
					append(upperProfile[i],rootProfile[i][j]);
				else
					append(lowerProfile[i],rootProfile[i][j]);
			}
		}

		// pruning empty segments
		TSegmentString _upperProfile;
		TSegmentString _lowerProfile;
		for(unsigned int i=0; i<length(rootProfile); i++){
			if(length(upperProfile[i])>0)
				appendValue(_upperProfile,upperProfile[i]);
			if(length(lowerProfile[i])>0)
				appendValue(_lowerProfile,lowerProfile[i]);
		}
		upperProfile= _upperProfile;
		lowerProfile= _lowerProfile;

		// realign
		heaviestCommonSubsequence(g, upperProfile, lowerProfile, tempProfile);
		sc= profileSumOfEdgeWeights(g,tempProfile);
		/*DEBUG
			printf("ir=%u sc=%2.f partition=[",ir,sc);
			for(unsigned int j=0; j<partition.size(); j++) if(partition[j]) printf("%u,",j);
			printf("] [");
			for(unsigned int j=0; j<partition.size(); j++) if(!partition[j]) printf("%u,",j);
			printf("]\n");*/

		if(sc>bsc){
			bsc= sc;
			rootProfile= tempProfile;
		}
	}
}


/*
 * Partition seqs into two profiles by systematically cutting all possible edges of the guiding tree.
 */
template<typename TStringSet, typename TCargo, typename TSpec, typename TGuideTree, typename TSegmentString>
inline void
iterativeRefinement_tree2(Graph<Alignment<TStringSet, TCargo, TSpec> > const& g,
						TGuideTree& guideTree,
						TSegmentString& rootProfile){
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Size<TGraph>::Type TSize;
	TStringSet seqSet= stringSet(g);
	TSize nLeaves= length(seqSet);
	TSegmentString upperProfile;
	TSegmentString lowerProfile;
	TSegmentString tempProfile;
	TCargo sc= profileSumOfEdgeWeights(g,rootProfile);
	TCargo bsc=sc;
	unsigned int seed=1;
	srand(seed);
	//DEBUG
		//printf("score=%.2f\n",sc);
	std::vector<bool> partition(nLeaves,false);


	// collect guiding tree nodes
	typedef typename VertexDescriptor<TGuideTree>::Type TVertexDescriptor;
	typedef String<TVertexDescriptor> TVertexString;
	typedef typename Iterator<TGuideTree, BfsIterator>::Type TBfsIterator;
	TSize nVertices = numVertices(guideTree)-1;
	TVertexString vertices;
	resize(vertices, nVertices);
	TVertexDescriptor rootVertex = getRoot(guideTree);
	TBfsIterator bfsIt(guideTree, rootVertex);
	goNext(bfsIt); // root excluded
	for(TSize i=0; !atEnd(bfsIt); goNext(bfsIt),i++){
		vertices[i]= *bfsIt;
	}
	/* DEBUG
	cout<<"# DEBUG guiding tree nodes in bfs order (no root, leaves marked with *)\n";
	for(unsigned int i=0; i<nVertices; i++){
		if(isLeaf(guideTree,vertices[i]))
			cout <<vertices[i]<<"*\n";
		else
			cout <<vertices[i]<<"\n";
	}*/



	for(unsigned int ir=0; ir<nVertices; ir++){
		// create partition
		String<TSize> group;
		collectLeaves(guideTree, vertices[ir], group);
		partition.assign(nLeaves,false);
		for(unsigned int i=0; i<length(group); i++)
			partition[group[i]]= true;

		clear(upperProfile); // clearing vertices from profiles and reserving memo for position strings
		clear(lowerProfile);
		resize(upperProfile,length(rootProfile));
		resize(lowerProfile,length(rootProfile));

		for(unsigned int i=0; i<length(rootProfile); i++){
			for(unsigned int j=0; j<length(rootProfile[i]); j++){
				TSize seqPos= idToPosition(seqSet,sequenceId(g,rootProfile[i][j]));
				if(partition[seqPos])
					append(upperProfile[i],rootProfile[i][j]);
				else
					append(lowerProfile[i],rootProfile[i][j]);
			}
		}

		// pruning empty segments
		TSegmentString _upperProfile;
		TSegmentString _lowerProfile;
		for(unsigned int i=0; i<length(rootProfile); i++){
			if(length(upperProfile[i])>0)
				appendValue(_upperProfile,upperProfile[i]);
			if(length(lowerProfile[i])>0)
				appendValue(_lowerProfile,lowerProfile[i]);
		}
		upperProfile= _upperProfile;
		lowerProfile= _lowerProfile;

		// realign
		heaviestCommonSubsequence(g, upperProfile, lowerProfile, tempProfile);
		sc= profileSumOfEdgeWeights(g,tempProfile);
		/*DEBUG
			printf("ir=%u sc=%2.f partition=[",ir,sc);
			for(unsigned int j=0; j<partition.size(); j++) if(partition[j]) printf("%u,",j);
			printf("] [");
			for(unsigned int j=0; j<partition.size(); j++) if(!partition[j]) printf("%u,",j);
			printf("]\n");*/

		if(sc>bsc){
			bsc= sc;
			rootProfile= tempProfile;
		}
	}
}


#endif
