#include <iostream>
#include <stdio.h>
#include <seqan/basic.h>
#include <seqan/graph_msa.h>
#include <seqan/modifier.h>
#include <seqan/misc/misc_cmdparser.h>
#include "msaParams.h"
#include "runMSA.h"
using namespace seqan;

void _initMsaParams(CommandLineParser& parser,MsaParams& msaParams) {

	// Set main options
	getOptionValueLong(parser, "seq",     msaParams.seqfile);
	getOptionValueLong(parser, "outfile", msaParams.outfile);
	getOptionValueLong(parser, "format",  msaParams.outputFormat);
	if(msaParams.outputFormat != "msf")
		msaParams.outputFormat= "fasta";

	// Set pairwise alignment generation options
	::std::string tmpVal;
	getOptionValueLong(parser, "method", tmpVal);
	unsigned int beg = 0;
	for(unsigned int i = 0; i<tmpVal.length(); ++i) {
		if (tmpVal[i] == ',') {
			if (tmpVal.substr(beg, i - beg) == "global") appendValue(msaParams.method, "global");
			else if (tmpVal.substr(beg, i - beg) == "local") appendValue(msaParams.method, "local");
			else if (tmpVal.substr(beg, i - beg) == "lcs") appendValue(msaParams.method, "lcs");
				beg = i + 1;
		}
	}
	if (beg != tmpVal.length()) {
		if (tmpVal.substr(beg, tmpVal.length() - beg) == "global") appendValue(msaParams.method, "global");
		else if (tmpVal.substr(beg, tmpVal.length() - beg) == "local") appendValue(msaParams.method, "local");
		else if (tmpVal.substr(beg, tmpVal.length() - beg) == "lcs") appendValue(msaParams.method, "lcs");
	}
	getOptionValueLong(parser, "nlocal", msaParams.nlocal);
	if(isSetLong(parser,"plocal"))
		msaParams.plocal=true;
	else
		msaParams.plocal=false;

	// alignment files
	getOptionValueLong(parser, "ali", tmpVal);
	beg = 0;
	for(unsigned int i = 0; i<tmpVal.length(); ++i) {
		if (tmpVal[i] == ',') {
		  appendValue(msaParams.alifiles, tmpVal.substr(beg, i - beg));
		  beg = i + 1;
		}
	}
	if (beg != tmpVal.length())
	appendValue(msaParams.alifiles, tmpVal.substr(beg, tmpVal.length() - beg));

	// lib files
	getOptionValueLong(parser, "lib", tmpVal);
	beg = 0;
	for(unsigned int i = 0; i<tmpVal.length(); ++i) {
		if (tmpVal[i] == ',') {
		  appendValue(msaParams.libfiles, tmpVal.substr(beg, i - beg));
		  beg = i + 1;
		}
	}
	if (beg != tmpVal.length())
	appendValue(msaParams.libfiles, tmpVal.substr(beg, tmpVal.length() - beg));

	// some default matche generation methods
	if(empty(msaParams.method) &&
		empty(msaParams.alifiles) &&
		empty(msaParams.libfiles)){
		appendValue(msaParams.method,"global");
		appendValue(msaParams.method,"local");
	}


	// Set edge weighting options
	getOptionValueLong(parser,"weight",msaParams.weight);
	if(msaParams.weight != "sm"
		&& msaParams.weight != "id")
		msaParams.weight= "sm";
	getOptionValueLong(parser,"smoffset",msaParams.smoffset);
	if(msaParams.smoffset<0)
		msaParams.smoffset=0;

	// set edge weighting by info source
	msaParams.infoWeight.resize(10,1);
	msaParams.infoWeight[INFO_SRC_GLOBAL]=    1.; //setting defaults
	msaParams.infoWeight[INFO_SRC_LOCAL]=     1.;
	msaParams.infoWeight[INFO_SRC_LCS]=       1.;
	msaParams.infoWeight[INFO_SRC_ALIGNMENT]= 1.;
	msaParams.infoWeight[INFO_SRC_LIBRARY]=   1.;
  getOptionValueLong(parser,"infoweight",tmpVal);
  std::string infoweight;
  std::string info;
  std::string weight;
  double weight_d;
  unsigned int eqpos= 0; // position of the equality sign
  beg = 0;
  for(unsigned int i = 0; i<tmpVal.length(); i++) {
    if (tmpVal[i] == ',') {
      infoweight= tmpVal.substr(beg,i-beg);
      eqpos= infoweight.find("=");
      info= infoweight.substr(0,eqpos);
      weight= infoweight.substr(eqpos+1);
      weight_d= atof(weight.c_str());
      beg = i + 1;
      if(weight_d>0){
	if(info.compare("global")==0){
	  msaParams.infoWeight[INFO_SRC_GLOBAL]= weight_d;
	}
	else if(info.compare("local")==0){
	  msaParams.infoWeight[INFO_SRC_LOCAL]= weight_d;
	}
	else if(info.compare("lcs")==0){
	  msaParams.infoWeight[INFO_SRC_LCS]= weight_d;
	}
	else if(info.compare("al")==0){
	  msaParams.infoWeight[INFO_SRC_ALIGNMENT]= weight_d;
	}
	else if(info.compare("li")==0){
	  msaParams.infoWeight[INFO_SRC_LIBRARY]= weight_d;
	}
      }
    }
  }
  if (beg != tmpVal.length()){
    infoweight= tmpVal.substr(beg,tmpVal.length()-beg);
    eqpos= infoweight.find("=");
    info= infoweight.substr(0,eqpos);
    weight= infoweight.substr(eqpos+1);
    weight_d= atof(weight.c_str());
    if(weight_d>0){
      if(info.compare("global")==0){
	msaParams.infoWeight[INFO_SRC_GLOBAL]= weight_d;
      }
      else if(info.compare("local")==0){
	msaParams.infoWeight[INFO_SRC_LOCAL]= weight_d;
      }
      else if(info.compare("lcs")==0){
	msaParams.infoWeight[INFO_SRC_LCS]= weight_d;
      }
      else if(info.compare("al")==0){
	msaParams.infoWeight[INFO_SRC_ALIGNMENT]= weight_d;
      }
      else if(info.compare("li")==0){
	msaParams.infoWeight[INFO_SRC_LIBRARY]= weight_d;
      }
    }
  }

  // Alignment Graph Transformation Options
	msaParams.consistent= "none"; //default
	getOptionValueLong(parser,"consistent",msaParams.consistent);
	if(msaParams.consistent != "triplet1"
	&& msaParams.consistent != "triplet2"
	&& msaParams.consistent != "maxflow")
  		msaParams.consistent= "none";

	getOptionValueLong(parser, "iconsistent", msaParams.iconsistent);
	if(msaParams.iconsistent>100){
		msaParams.iconsistent=100;
	}
	if(msaParams.iconsistent<1){
		msaParams.iconsistent=1;
	}

	if(isSetLong(parser,"cliquetr"))
		msaParams.cliquetr= true;
	else
		msaParams.cliquetr= false;

	// Set guide tree options
	getOptionValueLong(parser, "build", msaParams.build);
	if (msaParams.build != "nj"
		&& msaParams.build != "min"
		&& msaParams.build != "max"
		&& msaParams.build != "avg"
		&& msaParams.build != "wavg")
		msaParams.build= "min";

	getOptionValueLong(parser, "refine",msaParams.refine);
	if(msaParams.refine != "random"
		&& msaParams.refine != "tree1"
		&& msaParams.refine != "tree2")
		msaParams.refine= "none";
	msaParams.irefine=0;
	getOptionValueLong(parser, "irefine", msaParams.irefine);
	if(msaParams.irefine>1000){
	  msaParams.irefine=1000;
	}
	if(msaParams.irefine<0){
	  msaParams.irefine=0;
	}

	//verbal mode
	if(isSetLong(parser,"verbal"))
		msaParams.verbal=true;
	else
		msaParams.verbal=false;
}


void print(MsaParams& msaParams){
	printf("OPTIONS:\n");
	printf("%-14s %s\n","sequences",msaParams.seqfile.c_str());
	printf("%-14s %s\n","outfile",msaParams.outfile.c_str());
	printf("%-14s %s\n","format",msaParams.outputFormat.c_str());
	if(!empty(msaParams.method))
		printf("%-14s\n","method");
    for(unsigned int i=0; i<length(msaParams.method); i++)
		printf("%-14s %s\n","",msaParams.method[i].c_str());
	if(!empty(msaParams.method))
		printf("%-14s %d\n","nlocal",msaParams.nlocal);
	if(!empty(msaParams.method))
		printf("%-14s %s\n","plocal",(msaParams.plocal?"true":"false"));
	if(!empty(msaParams.alifiles))
		printf("%-14s\n","alifiles");
	for(unsigned int i=0; i<length(msaParams.alifiles); i++)
		printf("%-14s %s\n","",msaParams.alifiles[i].c_str());
	if(!empty(msaParams.libfiles))
		printf("%-14s\n","libfiles");
	for(unsigned int i=0; i<length(msaParams.libfiles); i++)
		printf("%-14s %s\n","",msaParams.libfiles[i].c_str());

	printf("%-14s %s\n","weight",msaParams.weight.c_str());

	printf("%-14s %d\n","smoffset",msaParams.smoffset);
	printf("%-14s\n","infoWeight");
	printf("%-14s %-10s %.2f\n","","global",msaParams.infoWeight[INFO_SRC_GLOBAL]);
	printf("%-14s %-10s %.2f\n","","local",msaParams.infoWeight[INFO_SRC_LOCAL]);
	printf("%-14s %-10s %.2f\n","","lcs",msaParams.infoWeight[INFO_SRC_LCS]);
	printf("%-14s %-10s %.2f\n","","alignment",msaParams.infoWeight[INFO_SRC_ALIGNMENT]);
	printf("%-14s %-10s %.2f\n","","library",msaParams.infoWeight[INFO_SRC_LIBRARY]);

	printf("%-14s %s\n","consistent",msaParams.consistent.c_str());
	printf("%-14s %d\n","iconsistent",msaParams.iconsistent);
	printf("%-14s %s\n","cliquetr",(msaParams.cliquetr?"true":"false"));
	printf("%-14s %s\n","guiding tree",msaParams.build.c_str());
	printf("%-14s %s\n","refine",msaParams.refine.c_str());
	printf("%-14s %d\n","irefine",msaParams.irefine);
	printf("%-14s %s\n","verbal",(msaParams.verbal?"true":"false"));
}

/*
 * Prints scores of a given scoring object for aminoacids
 */
template<typename TValue,
	 typename TSpec>
void printaa_score(Score<TValue,TSpec> const& score_type){
  std::cout<<"gap_open=   "<<score_type.data_gap_open<<"\n"
	   <<"gap_extend= "<<score_type.data_gap_extend<<"\n";
  std::cout<<"\t";
  int N= ValueSize<AminoAcid>::VALUE;
  for(int i=0; i<N; i++)
    std::cout<<"\t"<<AminoAcid(i);
  std::cout<<"\n";
  for(int i=0; i<N; i++){
    std::cout <<"\t"<<AminoAcid(i);
    for(int j=0; j<N; j++)
      std::cout<<"\t"<<(score(score_type,i,j));
    std::cout <<"\n";
  }
}


template <typename TSeqSet, typename TNameSet>
bool _loadSequences(TSeqSet& sequences,
		    TNameSet& fastaIDs,
		    const char *fileName){
  MultiFasta multiFasta;
  if (!open(multiFasta.concat, fileName, OPEN_RDONLY)) return false;
  AutoSeqFormat format;
  guessFormat(multiFasta.concat, format);
  split(multiFasta, format);
  unsigned seqCount = length(multiFasta);
  resize(sequences, seqCount, Exact());
  resize(fastaIDs, seqCount, Exact());
  for(unsigned i = 0; i < seqCount; ++i)
    {
      assignSeqId(fastaIDs[i], multiFasta[i], format);
      assignSeq(sequences[i], multiFasta[i], format);
    }
  return (seqCount > 0);
}

int main(int argc, const char *argv[]) {
  bool DEBUG= false;

  // Command line parsing
  CommandLineParser parser;

  addTitleLine(parser, "****************************************");
  addTitleLine(parser, "*             MMSA PROJECT             *");
  addTitleLine(parser, "* (c) Copyright 20011 by Pljusnin Ilja *");
  addTitleLine(parser, "* email: ilja.pljusnin@gmail.com       *");
  addTitleLine(parser, "****************************************");

  addUsageLine(parser, "-s <FASTA sequence file> [Options]");

  addSection(parser, "Main Options:");
  addOption(parser, addArgumentText(CommandLineOption("s", "seq", "file with sequences", OptionType::String), "<FASTA Sequence File>"));
  addOption(parser, addArgumentText(CommandLineOption("o", "outfile", "output filename", (int)OptionType::String, "out.fasta"), "<Filename>"));
  addOption(parser, addArgumentText(CommandLineOption("f", "format", "output format", (int)OptionType::String, "fasta"), "[fasta | msf]"));

  addSection(parser, "Pairwise Alignment Generation Options:");
  addOption(parser, CommandLineOption ("m", "method", "commaseparated list of pairwise alignment methods", OptionType::String));
  addHelpLine(parser, "global = Global alignments");
  addHelpLine(parser, "local = Local alignments");
  addHelpLine(parser, "lcs = Longest common subsequence");
  addHelpLine(parser, "when no other alignment methods/sources, default: global,local");
  addOption(parser, addArgumentText(CommandLineOption("nl", "nlocal", "number of local matches to use", (int)OptionType::Int, 4),"<Int>"));
  addOption(parser, CommandLineOption("pl", "plocal", "prune intersecting local alignments", OptionType::Boolean,false));
  addOption(parser, addArgumentText(CommandLineOption("al", "ali", "list of FASTA align files", OptionType::String), "<File1>,<File2>,..."));
  addOption(parser, addArgumentText(CommandLineOption("li", "lib", "list of T-Coffee libraries", OptionType::String), "<File1>,<File2>,..."));

  addSection(parser, "Edge Weighting Options:");
  addOption(parser, addArgumentText(CommandLineOption("w","weight", "method used to weight edges in pairwise alignments", OptionType::String,"sm"),"[sm|id]"));
  addHelpLine(parser, "sm = use values from substitution matrix (default)");
  addHelpLine(parser, "id = use sequence identity");
  addOption(parser, addArgumentText(CommandLineOption("smo", "smoffset", "substitution matrix offset used to rescore pairwise alignments", (int)OptionType::Int, 10), "<Int>"));
  addOption(parser, CommandLineOption("iw","infoweight", "specifying weight for each type of information source", OptionType::String));
  addHelpLine(parser, "this should be a string in format \'[global=w][,local=w][,lcs=w][,al=w][,li=w]\' for example \'global=2,local=1,li=100\'");

  addSection(parser, "Alignment Graph Transformation Options:");
  addOption(parser, addArgumentText(CommandLineOption("co", "consistent", "make Alignment Graph consistent", OptionType::String,"none"),"[none|triplet1|triplet2|maxflow]"));
  addHelpLine(parser, "triplet1 = triplet library extension (T-Coffee)");
  addHelpLine(parser, "triplet2 = triplet library extension with new edges introduced when implied by consistency");
  addHelpLine(parser, "maxflow = relative number of common neightbours (maxflow,gtg)");
  addOption(parser, addArgumentText(CommandLineOption("ico", "iconsistent", "iterate consistency transformation", (int)OptionType::Int, 1), "<Int>"));
  addOption(parser, CommandLineOption("cl", "cliquetr", "convert connected components to cliques", OptionType::Boolean,false));

  addSection(parser, "Scoring Options:");
  addOption(parser, addArgumentText(CommandLineOption("g", "gop", "gap open penalty", (int)OptionType::Int, -13), "<Int>"));
  addOption(parser, addArgumentText(CommandLineOption("e", "gex", "gap extension penalty", (int)OptionType::Int, -1), "<Int>"));
  addOption(parser, addArgumentText(CommandLineOption("ma", "matrix", "score matrix", (int)OptionType::String, "Blosum62"), "<Matrix file>"));
  addOption(parser, addArgumentText(CommandLineOption("ms", "msc", "match score", (int)OptionType::Int, 5), "<Int>"));
  addOption(parser, addArgumentText(CommandLineOption("mm", "mmsc", "mismatch penalty", (int)OptionType::Int, -4), "<Int>"));

  addSection(parser, "Guide Tree Options:");
  addOption(parser, addArgumentText(CommandLineOption("u", "usetree", "tree filename", OptionType::String), "<Newick guide tree>"));
  addOption(parser, addArgumentText(CommandLineOption("b", "build", "tree building method", (int)OptionType::String, "min"), "[nj, min, max, avg, wavg]"));
  addHelpLine(parser, "nj = Neighbor-joining");
  addHelpLine(parser, "min = UPGMA single linkage");
  addHelpLine(parser, "max = UPGMA complete linkage");
  addHelpLine(parser, "avg = UPGMA average linkage");
  addHelpLine(parser, "wavg = UPGMA weighted average linkage");
  addHelpLine(parser, "/*Neighbor-joining creates an");
  addHelpLine(parser, "  unrooted tree. We root that tree");
  addHelpLine(parser, "  at the last joined pair.*/");

  addSection(parser, "Postprocessing:");
  addOption(parser, addArgumentText(CommandLineOption("r", "refine", "iterative refinement", OptionType::String, "none"), "[none|random|tree1|tree2]"));
  addHelpLine(parser, "random = partition seqs randomly into two profiles that are realigned");
  addHelpLine(parser, "tree1  = partition seqs into two profiles by cutting a random edge of the guiding tree");
  addHelpLine(parser, "tree2  = partition seqs (#seqs-1) by cutting guiding tree edges in the breath-first order");
  addOption(parser, addArgumentText(CommandLineOption("ir", "irefine", "number of refinement iterations", (int)OptionType::Int, 0),"<Int>"));
  addOption(parser, CommandLineOption("v", "verbal", "prints input parameters", OptionType::Boolean,false));

  if (argc == 1){
    shortHelp(parser, std::cerr);// print short help and exit
    return 0;
  }

  if (!parse(parser, argc, argv, ::std::cerr)) return 1;
  if (isSetLong(parser, "help") || isSetLong(parser, "version")) return 0;// print help or version and exit


  // parsing options
  MsaParams msaParams;
  _initMsaParams(parser, msaParams);
  if(msaParams.verbal){
    print(msaParams);
  }
  // reading sequences
  typedef String<AminoAcid> TSequence;
  StringSet<TSequence, Owner<> > sequenceSet;
  StringSet<String<char> > sequenceNames;
  if(!_loadSequences(sequenceSet, sequenceNames, msaParams.seqfile.c_str())){
    std::cerr << "failed to load sequences: exiting\n";
    exit(1);
  }

  if(DEBUG){
    std::cout << "# DEBUG sequences:\n";
    for(unsigned int i=0; i<length(sequenceNames); i++){
      std::cout << ">"<<sequenceNames[i]<<"\n";
      std::cout << sequenceSet[i]<<"\n";
    }
  }

  // Alignment Graph
  Graph<Alignment<StringSet<TSequence, Dependent<> >, void, WithoutEdgeId> > gAlign;

  // reading/initializing scoring matrix
  // code is forked here depending on the type of the matrix
  String<char> matrix;
  getOptionValueLong(parser, "matrix", matrix);
  if (isSetLong(parser, "matrix")) {
    Score<int, ScoreMatrix<> > sc;
    loadScoreMatrix(sc, matrix);
    getOptionValueLong(parser, "gop", sc.data_gap_open);
    getOptionValueLong(parser, "gex", sc.data_gap_extend);
    if(DEBUG){
      std::cout << "# DEBUG score:\n";
      printaa_score(sc);
    }

    runMSA(gAlign, sequenceSet, sequenceNames, msaParams, sc, DEBUG);
  } else {
    Blosum62 sc;
    getOptionValueLong(parser, "gop", sc.data_gap_open);
    getOptionValueLong(parser, "gex", sc.data_gap_extend);
    if(DEBUG){
      std::cout << "# DEBUG score:\n";
      printaa_score(sc);
    }

    runMSA(gAlign, sequenceSet, sequenceNames, msaParams, sc, DEBUG);
  }


  // Alignment output
  if (msaParams.outputFormat == "msf") {
    FILE* strmWrite = fopen(msaParams.outfile.c_str(), "w");
    write(strmWrite, gAlign, sequenceNames, MsfFormat());
    fclose(strmWrite);
  }
  else{
    FILE* strmWrite = fopen(msaParams.outfile.c_str(), "w");
    write(strmWrite, gAlign, sequenceNames, FastaFormat());
    fclose(strmWrite);
  }

  return 0;
}
