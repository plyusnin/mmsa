#ifndef MAXFLOWCONSISTENCY_H
#define MAXFLOWCONSISTENCY_H
#include <iostream>
#include <algorithm>
#include <seqan/basic.h>
#include <seqan/graph_msa.h>
using namespace seqan;


template<typename TStringSet, 
	 typename TCargo, 
	 typename TSpec>
inline void
maxflowConsistency_simle(Graph<Alignment<TStringSet, TCargo, TSpec> >& g){
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Size<TGraph>::Type TSize;
	typedef typename VertexDescriptor<TGraph>::Type TVertexDescriptor;
	typedef typename Iterator<TGraph, EdgeIterator>::Type TEdgeIterator;
	typedef typename Iterator<TGraph, OutEdgeIterator>::Type TOutEdgeIterator;
	typedef String<TVertexDescriptor> TVertexString;
	typedef typename Iterator<TVertexString,Standard>::Type TVertexStringIterator;
	
	// Store all edges
	typedef std::pair<TVertexDescriptor, TVertexDescriptor> TNewEdge;
	typedef std::map<TNewEdge, TCargo> TEdgeMap;
	typedef typename TEdgeMap::iterator TEdgeMapIter;
	TEdgeMap newEMap;
	//typedef __EdgeCargo<TVertexDescriptor, TCargo> TEdge;
	//typedef String<TEdge> TEdgeString;
	TEdgeIterator itE(g);
	for(;!atEnd(itE);goNext(itE)) {
	  TVertexDescriptor sV = sourceVertex(itE);
	  TVertexDescriptor tV = targetVertex(itE);
	  TCargo c = cargo(*itE);
	  //appendValue(fullEdges, TEdge(sV, tV, c), Generous());
	  //appendValue(fullEdges, TEdge(tV, sV, c), Generous());
	  newEMap.insert(std::make_pair(TNewEdge(sV, tV), c));
	}
	//clearEdges(g);
	//::std::sort(begin(fullEdges, Standard()), end(fullEdges, Standard()), __LessEdgeCargo<TVertexDescriptor, TCargo>() );
	

	// Perform maxflow consistency	
	TVertexString qN;
	TVertexString sN;
	TSize common_neigh;
	TSize qneigh_nc;
	TSize sneigh_nc;
	TCargo w=0;

	for(TEdgeMapIter itE = newEMap.begin(); itE != newEMap.end(); ++itE){
	  //	  addEdge(g, (*itE).first.first, (*itE).first.second, (*itE).second);	  
	  common_neigh=0;
	  qneigh_nc=0;
	  sneigh_nc=0;
	  TVertexDescriptor qV = (*itE).first.first;
	  TVertexDescriptor sV = (*itE).first.second;
	  TOutEdgeIterator itqOE(g,qV);
	  clear(qN);
	  for(;!atEnd(itqOE);goNext(itqOE)){
	    appendValue(qN,targetVertex(itqOE),Generous());
	  }
	  TOutEdgeIterator itsOE(g,sV);
	  clear(sN);
	  for(;!atEnd(itsOE);goNext(itsOE)){
	    appendValue(sN,targetVertex(itsOE),Generous());
	  }
	  
	  std::sort(begin(qN,Standard()), end(qN,Standard())); // using < operator
	  std::sort(begin(sN,Standard()), end(sN,Standard())); // using < operator	  
	  TVertexStringIterator i= begin(qN,Standard());
	  TVertexStringIterator i_end= end(qN,Standard());
	  TVertexStringIterator j= begin(sN,Standard());
	  TVertexStringIterator j_end= end(sN,Standard());
	  qneigh_nc= length(qN);
	  sneigh_nc= length(sN);

	  while( i!=i_end && j!=j_end){
	    if((*i) < (*j))
	      ++i;
	    else if((*j) < (*i))
	      ++j;
	    else{
	      if((*i) == (*j))
		common_neigh++;
	      ++i;
	      ++j;
	    }
	  }

	  //DEBUG
	  //std::cout << "qid="<<sequenceId(g,qV)<<" sid="<<sequenceId(g,sV)<<" qneigh_nc="<<qneigh_nc<<" sneigh_nc="<<sneigh_nc<<" common_neigh="<<common_neigh<<"\n"
          //          << label(g,qV)<<"\n"
          //          << label(g,sV)<<"\n";

	  if(qneigh_nc + sneigh_nc - common_neigh >=2){
	    w= (100*(common_neigh+2)) / (qneigh_nc + sneigh_nc - common_neigh);
	  }
	  else{
	    /*
	     * We interpret this case as query and subject being the only residues in the neighbourhood
	     */
	    w= 100;
	  }
	  (*itE).second= w;
	  
	}

	// Insert edges
	clearEdges(g);
	clear(qN);
	clear(sN);
	for(TEdgeMapIter itE = newEMap.begin(); itE != newEMap.end(); ++itE){
	  addEdge(g, (*itE).first.first, (*itE).first.second, (*itE).second);	  
	}
}



template<typename TStringSet, 
	 typename TCargo, 
	 typename TSpec>
inline void
maxflowConsistency_fast(Graph<Alignment<TStringSet, TCargo, TSpec> >& g){
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Size<TGraph>::Type TSize;
	typedef typename VertexDescriptor<TGraph>::Type TVertexDescriptor;
	typedef typename Iterator<TGraph, EdgeIterator>::Type TEdgeIterator;
	typedef typename Iterator<TGraph, OutEdgeIterator>::Type TOutEdgeIterator;
	typedef String<TVertexDescriptor> TVertexString;
	typedef typename Iterator<TVertexString,Standard>::Type TVertexStringIterator;
	typedef typename Iterator<TGraph, VertexIterator>::Type TVertexIterator;

	// Store all edges
	typedef std::pair<TVertexDescriptor, TVertexDescriptor> TNewEdge;
	typedef std::map<TNewEdge, TCargo> TEdgeMap;
	typedef typename TEdgeMap::iterator TEdgeMapIter;
	TEdgeMap newEMap;
	TEdgeIterator itE(g);
	for(;!atEnd(itE);goNext(itE)) {
	  TVertexDescriptor sV = sourceVertex(itE);
	  TVertexDescriptor tV = targetVertex(itE);
	  TCargo c = cargo(*itE);
	  newEMap.insert(std::make_pair(TNewEdge(sV, tV), c));
	}
	
	// get neighbourhoods for each vertex
	typedef std::map<TVertexDescriptor,TVertexString> TNeighMap;
	typedef typename TNeighMap::iterator TNeighMapIterator;
	TNeighMap neighMap;
	TVertexIterator itV(g);
	for(;!atEnd(itV);goNext(itV)){
	  TOutEdgeIterator itqOE(g,*itV);
	  TVertexString qN;
	  for(;!atEnd(itqOE);goNext(itqOE)){
	    appendValue(qN,targetVertex(itqOE),Generous());
	  }
	  neighMap.insert(std::make_pair(*itV,qN));
	}

	// Perform maxflow consistency	
	TVertexString qN;
	TVertexString sN;
	TSize common_neigh;
	TSize qneigh_nc;
	TSize sneigh_nc;
	TCargo w=0;

	for(TEdgeMapIter itE = newEMap.begin(); itE != newEMap.end(); ++itE){
	  common_neigh=0;
	  qneigh_nc=0;
	  sneigh_nc=0;
	  TVertexDescriptor qV = (*itE).first.first;
	  TVertexDescriptor sV = (*itE).first.second;
	  TNeighMapIterator itQN= neighMap.find(qV);
	  TNeighMapIterator itSN= neighMap.find(sV);
	  if(itQN == neighMap.end() || itSN == neighMap.end()){
	    std::cerr << "maxflowConsistency(): somehow no neighbourhood found for query="<<qV<<" or subject="<<sV<<" vertex: exiting\n";
	    exit(1);
	  }
	  qN= (*itQN).second;
	  sN= (*itSN).second;
	  std::sort(begin(qN,Standard()), end(qN,Standard())); // using < operator
	  std::sort(begin(sN,Standard()), end(sN,Standard())); // using < operator	  
	  TVertexStringIterator i= begin(qN,Standard());
	  TVertexStringIterator i_end= end(qN,Standard());
	  TVertexStringIterator j= begin(sN,Standard());
	  TVertexStringIterator j_end= end(sN,Standard());
	  qneigh_nc= length(qN);
	  sneigh_nc= length(sN);

	  while( i!=i_end && j!=j_end){
	    if((*i) < (*j))
	      ++i;
	    else if((*j) < (*i))
	      ++j;
	    else{
	      if((*i) == (*j))
		common_neigh++;
	      ++i;
	      ++j;
	    }
	  }

	  //DEBUG
	  //std::cout << "qid="<<sequenceId(g,qV)<<" sid="<<sequenceId(g,sV)<<" qneigh_nc="<<qneigh_nc<<" sneigh_nc="<<sneigh_nc<<" common_neigh="<<common_neigh<<"\n"
          //          << label(g,qV)<<"\n"
          //          << label(g,sV)<<"\n";

	  if(qneigh_nc + sneigh_nc - common_neigh >=2){
	    w= (100*(common_neigh+2)) / (qneigh_nc + sneigh_nc - common_neigh);
	  }
	  else{
	    /*
	     * We interpret this case as query and subject being the only residues in the neighbourhood
	     */
	    w= 100;
	  }
	  (*itE).second= w;
	  
	}

	// Insert edges
	clearEdges(g);
	clear(qN);
	clear(sN);
	for(TEdgeMapIter itE = newEMap.begin(); itE != newEMap.end(); ++itE){
	  addEdge(g, (*itE).first.first, (*itE).first.second, (*itE).second);	  
	}
}




#endif
