#ifndef MINITREE_H_
#define MINITREE_H_
#include <vector>
#include <queue>
#include <iostream>
using namespace std;

/*
 * Minimal code for representing clustering trees.
 * Also some code for manipulating such trees.
 */
typedef unsigned int TVertex;
typedef double TCargo;
struct Edge{
  TVertex source;
  TVertex target;
  TCargo cargo;
  Edge(){
    source=0;
    target=0;
    cargo=0;
  };
  Edge(TVertex v1, TVertex v2, TCargo c){
    source= v1;
    target= v2;
    cargo= c;
  };
  Edge& operator=(const Edge& e){
    if(this != &e){
      this->source= e.source;
      this->target= e.target;
      this->cargo=  e.cargo;
    }
    return *this;
  };
};

/* Operator for printing compact representation of an Edge.
 */
std::ostream & operator<<(std::ostream & output, Edge const& edge){
  output << "s="<<edge.source<<",t="<<edge.target<<",c="<<edge.cargo;
  return output;
}


// IMPLEMENTATION
void printTree(TVertex rootVertex, 
	       vector<Edge>& treeEdges, 
	       TVertex leafN);

void printMatlabTree(TVertex rootVertex, 
		     vector<Edge>& treeEdges, 
		     TVertex leafN);

void printTree(TVertex rootVertex, 
	       vector<Edge>& treeEdges, 
	       TVertex leafN){
  queue<TVertex> vqueue;
  vector<TVertex> interVertices;
  // traversing the tree to pick the vertices
  vqueue.push(rootVertex);
  TVertex v;
  Edge e;
  while( !vqueue.empty() ){
    v= vqueue.front();vqueue.pop();
    if(v>=leafN){
      e= treeEdges[v-leafN];
      interVertices.push_back(v);
      vqueue.push(e.source);
      vqueue.push(e.target);
    }
  }
  // printing
  sort(interVertices.begin(),interVertices.end());
  cout<<"vertex\tleft_child\tright_child\tcargo\n";
  for(vector<TVertex>::iterator it=interVertices.begin(); it<interVertices.end(); it++){
    e= treeEdges[*it-leafN];
    cout << (*it)<<"\t"<<(e.source)<<"\t"<<(e.target)<<"\t"<<(e.cargo)<<"\n";
  }
}


void printMatlabTree(TVertex rootVertex, 
		     vector<Edge>& treeEdges, 
		     TVertex leafN){
  queue<TVertex> vqueue;
  vector<TVertex> interVertices;
  // traversing the tree to pick the vertices
  vqueue.push(rootVertex);
  TVertex v;
  Edge e;
  while( !vqueue.empty() ){
    v= vqueue.front();vqueue.pop();
    if(v>=leafN){
      e= treeEdges[v-leafN];
      interVertices.push_back(v);
      vqueue.push(e.source);
      vqueue.push(e.target);
    }
  }
  // printing
  sort(interVertices.begin(),interVertices.end());
  cout<<"Z=[";
  for(vector<TVertex>::iterator it=interVertices.begin(); it<interVertices.end(); it++){
    e= treeEdges[*it-leafN];
    cout <<(e.source+1)<<" "<<(e.target+1)<<" "<<(e.cargo)<<";\n";
  }
  cout << "];\n";
  cout << "dendrogram(Z);\n\n";
}

#endif

