#ifndef MSAPARAMS_H
#define MSAPARAMS_H
#include <seqan/align.h>
#include <vector>
using namespace seqan;

// constants
const unsigned int INFO_SRC_GLOBAL=    0;
const unsigned int INFO_SRC_LOCAL=     1;
const unsigned int INFO_SRC_LCS=       2;
const unsigned int INFO_SRC_ALIGNMENT= 3;
const unsigned int INFO_SRC_LIBRARY=   4;

struct MsaParams {
public:
	// main options
	std::string seqfile;// Sequence file name
	std::string outfile;// Output file name
	std::string outputFormat;
	seqan::String<std::string> method; // methods for pairwise alignment generation
	seqan::String<std::string> alifiles; // alignment files
	seqan::String<std::string> libfiles; // t-coffee lib files
	unsigned int nlocal;
	bool plocal;
	std::string weight; // // Edge Weighting Options: sm or id
	std::vector<double> infoWeight;
	int smoffset; // substitution matrix offset used to rescore pairwise alignments

	//Graph transformation options
	std::string consistent; // none|triplet1|triplet2|maxflow
	unsigned int iconsistent; // number of times to repeat consistency transformation
	bool cliquetr; // true|false

	// method to compute guiding tree
	// nj: Neighbor-joining
	// min: UPGMA single linkage
	// max: UPGMA complete linkage
	// avg: UPGMA average linkage
	// wavg: UPGMA weighted average linkage
	std::string build;

	// iterative refinement
	std::string refine;
	unsigned int irefine;

	//verbal mode
	bool verbal;
};

#endif
