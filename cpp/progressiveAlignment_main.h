#ifndef PROGRESSIVE_ALIGNMENT_MAIN_H
#define PROGRESSIVE_ALIGNMENT_MAIN_H
#include <seqan/graph_types.h>
#include <seqan/graph_msa/graph_align_tcoffee_progressive.h>
using namespace seqan;

/**
.Function.progressiveAlignment:
..summary:Performs a progressive alignment.
..cat:Graph
..signature:
progressiveAlignment(inputGraph, guideTree, outputGraph)
..param.inputGraph:The alignment graph with multiple sequence information.
...type:Spec.Alignment Graph
..param.guideTree:A guide tree.
...type:Spec.Tree
..param.outputGraph:An alignment graph for the final MSA.
...type:Spec.Alignment Graph
..returns:void
*
* Modified version of
seqan::graph_msa::graph_align_tcoffee_progressive.h:progressiveAlignment(..)
* that omits the creation of printable Alignment Graph. This step is shifted to the runMSA(..).
*
*/
template<typename TStringSet, typename TCargo, typename TSpec, typename TGuideTree, typename TVertexDescriptor>
inline void
progressiveAlignment_main(Graph<Alignment<TStringSet, TCargo, TSpec> >& g,
					 TGuideTree& tree,
					 String<String<TVertexDescriptor> >& profileOut){
	SEQAN_CHECKPOINT
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Size<TGraph>::Type TSize;
	//typedef typename VertexDescriptor<TGuideTree>::Type TVertexDescriptor;
	typedef typename Iterator<TGuideTree, BfsIterator>::Type TBfsIterator;
	typedef typename Iterator<TGuideTree, AdjacencyIterator>::Type TAdjacencyIterator;
	typedef String<TVertexDescriptor> TVertexString;
	typedef String<TVertexString> TSegmentString;

	// Initialization
	TVertexDescriptor rootVertex = getRoot(tree);
	TSize nVertices = numVertices(tree);

	// Vertices in reversed bfs order
	TVertexString vertices;
	resize(vertices, nVertices);

	// All Strings of Strings of vertices for each node of the guide tree
	String<TSegmentString> segString;
	resize(segString, nVertices);

	// Walk through the tree in bfs order
	typedef typename Iterator<TVertexString, Standard>::Type TVertexIter;
	TVertexIter itVert = begin(vertices, Standard());
	TVertexIter itVertEnd = end(vertices, Standard());
	--itVertEnd;
	TBfsIterator bfsIt(tree, rootVertex);
	for(;!atEnd(bfsIt);goNext(bfsIt), --itVertEnd)
		*itVertEnd = *bfsIt;

	// Progressive alignment
	itVert = begin(vertices, Standard());
	itVertEnd = end(vertices, Standard());
	for(;itVert != itVertEnd; ++itVert) {
		if(isLeaf(tree, *itVert)) _buildLeafString(g, *itVert, segString[*itVert]);
		else {
			// Align the two children (Binary tree)
			TAdjacencyIterator adjIt(tree, *itVert);
			TVertexDescriptor child1 = *adjIt; goNext(adjIt);
			heaviestCommonSubsequence(g, segString[child1], segString[*adjIt], segString[*itVert]);
			clear(segString[child1]);
			clear(segString[*adjIt]);
		}
	}
	profileOut= segString[rootVertex];
}

#endif
