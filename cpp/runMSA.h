#ifndef RUNMSA_H
#define RUNMSA_H

#include <iostream>
#include <string>
#include <stdio.h>
#include <seqan/basic.h>
#include <seqan/graph_msa.h>
#include <seqan/graph_msa/graph_align_tcoffee_base.h>
#include <time.h>
#include "util.h"
#include "msaParams.h"
#include "maxflowConsistency.h"
#include "cliqueTransformation.h"
#include "tripletLibraryExtension1.h"
#include "tripletLibraryExtension2_3.h"
#include "progressiveAlignment_main.h"
#include "iterativeRefinement.h"

using namespace seqan;

template<typename TFragment, typename TSpec1, typename TString, typename TSpec2, typename TValue,  typename TSpec, typename TSize>
inline void
_setSimAndDistValues(String<TFragment, TSpec1>& matches,
		     StringSet<TString, TSpec2>& pairSet,
		     String<TValue, TSpec>& dist,
		     String<TValue, TSpec>& sim,
		     TSize i,
		     TSize j,
		     TSize nseq,
		     TSize from){
  //  SEQAN_CHECKPOINT
  typedef typename Position<String<TFragment, TSpec1> >::Type TPos;

  // Determine a sequence weight
  TValue matchLen = 0;
  TValue overlapLen = 0;
  TValue alignLen = 0;
  getAlignmentStatistics(matches, pairSet, (TPos) from, (TPos) length(matches),  matchLen, overlapLen, alignLen);

  // Calculate sequence similarity
  TValue val= (TValue) ((double) matchLen / (double) overlapLen) * ((double) overlapLen / (double) alignLen);
  if(i < j){
    sim[i*nseq+j]= val;
    dist[i* nseq+j]= 1 - val* (double) SEQAN_DISTANCE_UNITY;
  }
  else{
    sim[j*nseq+i]= val;
    dist[j* nseq+i]= 1 - val* (double) SEQAN_DISTANCE_UNITY;
  }

}

/*
 * Printing Alignment Graph edges in the pairwise order of the sequences.
 */
template<typename TStringSet,
	 typename TCargo,
	 typename TSpec>
void printEdges(Graph<Alignment<TStringSet, TCargo, TSpec> >  const& gAlign){
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Iterator<TGraph,EdgeIterator>::Type TEdgeIterator;
	typedef typename VertexDescriptor<TGraph>::Type TVertexDescriptor;
	typedef typename Size<TStringSet>::Type TSize;
	TStringSet seqSet= getStringSet(gAlign);
	TSize nseq= length(seqSet);
	TEdgeIterator itE(gAlign);

	std::cout<<"sseq\ttseq\tsind\ttind\tslabel\ttlabel\tscore\n";
	for(TSize si=0; si<nseq; si++){
		for(TSize ti=si+1; ti<nseq; ti++){
			goBegin(itE);
			for(;!atEnd(itE);goNext(itE)) {
				TVertexDescriptor sV = sourceVertex(itE);
				TVertexDescriptor tV = targetVertex(itE);
				if((sequenceId(gAlign,sV)==si && sequenceId(gAlign,tV)==ti)
					||(sequenceId(gAlign,sV)==ti && sequenceId(gAlign,tV)==si)){
					if(sequenceId(gAlign,sV)==ti){
						TVertexDescriptor temp= sV;
						sV= tV;
						tV= temp;
					}
					std::cout<< si <<"\t"
						<< ti <<"\t"
						<< fragmentBegin(gAlign,sV)<<":"<<(fragmentBegin(gAlign,sV)+fragmentLength(gAlign,sV))<<"\t"
						<< fragmentBegin(gAlign,tV)<<":"<<(fragmentBegin(gAlign,tV)+fragmentLength(gAlign,tV))<<"\t"
						<< label(gAlign,sV)<<"\t"
						<< label(gAlign,tV)<<"\t"
						<< cargo(*itE)<<"\n";
				}
			}
		}
	}
}

template<typename TString,
	 typename TSpec,
	 typename TSegmentMatches,
	 typename TScores,
	 typename TSize>
void printSegmentMatches(StringSet<TString,TSpec> const& str,
			 TSegmentMatches const& matches,
			 TScores const& scores,
			 TSize from,
			 TSize to){
	typedef typename Iterator<TSegmentMatches, Standard>::Type TSegmentMatchIter;
	typedef typename Iterator<TScores, Standard>::Type TScoreIter;
	typedef StringSet<TString,TSpec> TStringSet;
	typedef typename Id<typename Value<TSegmentMatches>::Type>::Type TId;

	typename Infix<TString >::Type qinf;
	typename Infix<TString >::Type sinf;
	TSegmentMatchIter itMatch= begin(matches);
	TSegmentMatchIter itMatchEnd= begin(matches);
	TScoreIter itScore = begin(scores, Standard());
	TScoreIter itScoreEnd = begin(scores, Standard());
	itMatch+= from;
	itMatchEnd+= to;
	itScore+= from;
	itScoreEnd+= to;

	unsigned int s1,s2,t1,t2;

	std::cout<<"sseq\ttseq\tsind\ttind\tslabel\ttlabel\tscore\n";
	for(unsigned int i=1;itMatch != itMatchEnd && itScore != itScoreEnd; i++,itMatch++,itScore++){
		s1= itMatch->begin1;
		s2= (itMatch->begin1)+(itMatch->len);
		t1= itMatch->begin2;
		t2= (itMatch->begin2)+(itMatch->len);
		std::cout << (itMatch->seqId1)
					<<"\t"<<(itMatch->seqId2)
					<<"\t"<<s1<<":"<<s2
					<<"\t"<<t1<<":"<<t2
					<<"\t"<<infix(str[idToPosition(str,itMatch->seqId1)],s1,s2)
					<<"\t"<<infix(str[idToPosition(str,itMatch->seqId2)],t1,t2)
					<<"\t"<<(*itScore)<<"\n";
	}
}

template<typename TString,
	 typename TSpec,
	 typename TpList,
	 typename TScore,
	 typename TSegmentMatches,
	 typename TScoreValues,
	 typename TDistance,
	 typename TSimilarity>
inline void
appendSegmentMatches_gotoh(StringSet<TString,TSpec> const& str,
			   TpList const& pList,
			   TScore const& score_type,
			   TSegmentMatches& matches,
			   TScoreValues& scores,
			   TDistance& dist,
			   TSimilarity& sim){

  //  SEQAN_CHECKPOINT
  typedef StringSet<TString,TSpec > TStringSet;
  typedef typename Id<TStringSet>::Type TId;
  typedef typename Size<TStringSet>::Type TSize;
  typedef typename Value<TScoreValues>::Type TScoreValue;
  typedef typename Value<TSegmentMatches>::Type TSegmentMatch;
  typedef typename Iterator<TpList, Standard>::Type TPairIter;

  // Initialization
  TSize nseq = length(str);
  //__resizeWithRespectToDistance(dist, nseq);
  resize(dist, nseq * nseq);
  resize(sim, nseq * nseq);

  // Pairwise alignments
  TPairIter itPair = begin(pList, Standard());
  TPairIter itPairEnd = end(pList, Standard());
  for(;itPair != itPairEnd; ++itPair) {
    // Make a pairwise string-set
    TStringSet pairSet;
    TId id1 = positionToId(str, *itPair); ++itPair;
    TId id2 = positionToId(str, *itPair);
    assignValueById(pairSet, const_cast<TStringSet&>(str), id1);
    assignValueById(pairSet, const_cast<TStringSet&>(str), id2);

    // Alignment
    TSize from = length(matches);
    //  globalAlignment(matches, str, blosum62, AlignConfig<0,0,0,0>(), Gotoh() );
    TScoreValue myScore = globalAlignment(matches, pairSet, score_type, AlignConfig<0,0,0,0>(), Gotoh() );
    TSize to = length(matches);

    // Record the scores
    resize(scores, to);
    typedef typename Iterator<TScoreValues, Standard>::Type TScoreIter;
    TScoreIter itScore = begin(scores, Standard());
    TScoreIter itScoreEnd = end(scores, Standard());
    itScore+=from;
    for(;itScore != itScoreEnd; ++itScore) *itScore = myScore; // setting the Gotoh score as the score for all matches

    // Set query-subject similarity to sequence_identity and distance to (1 - sequence_identity)
    _setSimAndDistValues(matches, pairSet, dist, sim, (TSize) *(itPair-1), (TSize) *itPair, (TSize) nseq, (TSize)from);
  }
  // cosmetic: setting main diagonal and upper triangle of the sim and the dist matrices
  for(TSize qi=0; qi<nseq; qi++){
    sim[nseq*qi+qi]= 1;
    dist[nseq*qi+qi]= 0;
    for(TSize si=qi+1; si<nseq; si++){
      sim[nseq*si+qi]= sim[nseq*qi+si];
      dist[nseq*si+qi]= dist[nseq*qi+si];
    }
  }
}



template<typename TString,
	 typename TSpec,
	 typename TpList,
	 typename TScore,
	 typename TSegmentMatches,
	 typename TScoreValues>
inline void
appendSegmentMatches_swclump(StringSet<TString,TSpec> const& str,
							TpList const& pList,
							TScore const& score_type,
							TSegmentMatches& matches,
							TScoreValues& scores,
							unsigned int nlocal,
							bool plocal){

	typedef StringSet<TString,TSpec > TStringSet;
	typedef typename Id<TStringSet>::Type TId;
	typedef typename Size<TStringSet>::Type TSize;
	typedef typename Iterator<TpList, Standard>::Type TPairIter;
	typedef typename Iterator<TSegmentMatches, Standard>::Type TSegmentMatchIter;
	typedef typename Iterator<TScoreValues, Standard>::Type TScoreIter;

	// Pairwise alignments
	TPairIter itPair = begin(pList, Standard());
	TPairIter itPairEnd = end(pList, Standard());
	for(;itPair != itPairEnd; ++itPair) {
		// Make a pairwise string-set
		TStringSet pairSet;
		TId id1 = positionToId(str, *itPair); ++itPair;
		TId id2 = positionToId(str, *itPair);
		assignValueById(pairSet, const_cast<TStringSet&>(str), id1);
		assignValueById(pairSet, const_cast<TStringSet&>(str), id2);
		TSize from= length(matches);
		multiLocalAlignment(pairSet, matches, scores, score_type, nlocal, SmithWatermanClump() );
		TSize to= length(matches);

		// deleting intersecting alignments
		if(plocal){
			TSegmentMatchIter itMatch= (begin(matches)+from);
			TSegmentMatchIter itMatchEnd= end(matches);
			TScoreIter itScore= (begin(scores)+from);
			TSegmentMatches matches_pruned;
			TScoreValues scores_pruned;
			append(matches_pruned,*itMatch);
			append(scores_pruned,*itScore);
			itMatch++;
			itScore++;
			unsigned int m_sb,m_se,m_tb,m_te,m2_sb,m2_se,m2_tb,m2_te; // m=match,s=source,t=target,b=begin,e=end
			bool intersect;
			for(;itMatch != itMatchEnd; itMatch++,itScore++){
				intersect=false;
				m_sb= itMatch->begin1;
				m_se= (itMatch->begin1)+(itMatch->len)-1;
				m_tb= itMatch->begin2;
				m_te= (itMatch->begin2)+(itMatch->len)-1;

				for(TSegmentMatchIter itMatch2= begin(matches_pruned); itMatch2!= end(matches_pruned); itMatch2++){
					m2_sb= itMatch2->begin1;
					m2_se= (itMatch2->begin1)+(itMatch2->len)-1;
					m2_tb= itMatch2->begin2;
					m2_te= (itMatch2->begin2)+(itMatch2->len)-1;
					if((m_sb>=m2_sb && m_sb<=m2_se)
					|| (m_se>=m2_sb && m_se<=m2_se)
					|| (m2_sb>=m_sb && m2_sb<=m_se)
					|| (m2_se>=m_sb && m2_se<=m_se)
					|| (m_tb>=m2_tb && m_tb<=m2_te)
					|| (m_te>=m2_tb && m_te<=m2_te)
					|| (m2_tb>=m_tb && m2_tb<=m_te)
					|| (m2_te>=m_tb && m2_te<=m_te)){
						intersect=true;
						break;
					}
				}
				if(!intersect){
					append(matches_pruned,*itMatch);
					append(scores_pruned,*itScore);
				}
			}
			erase(matches,from,to);
			erase(scores,from,to);
			itMatch= begin(matches_pruned);
			itScore= begin(scores_pruned);
			itMatchEnd= end(matches_pruned);
			for(;itMatch!= itMatchEnd; itMatch++,itScore++){
				append(matches,*itMatch);
				append(scores,*itScore);
			}
		}
	}
}


template<typename TString,
	 typename TSpec,
	 typename TpList,
	 typename TScore,
	 typename TSegmentMatches,
	 typename TScoreValues>
inline void
appendSegmentMatches_lcs(StringSet<TString,TSpec> const& str,
			 TpList const& pList,
			 TScore const& score_type,
			 TSegmentMatches& matches,
			 TScoreValues& scores){
  typedef StringSet<TString,TSpec > TStringSet;
  typedef typename Id<TStringSet>::Type TId;
  typedef typename Size<TStringSet>::Type TSize;
  typedef typename Iterator<TpList, Standard>::Type TPairIter;

    // Pairwise longest common subsequence
  TPairIter itPair = begin(pList, Standard());
  TPairIter itPairEnd = end(pList, Standard());
  for(;itPair != itPairEnd; ++itPair) {
    TStringSet pairSet;
    TId id1 = positionToId(str, *itPair); ++itPair;
    TId id2 = positionToId(str, *itPair);
    assignValueById(pairSet, const_cast<TStringSet&>(str), id1);
    assignValueById(pairSet, const_cast<TStringSet&>(str), id2);

    // Lcs between first and second string
    TSize from = length(matches);
    globalAlignment(matches, pairSet, Lcs());
    TSize to = length(matches);

    // Record the scores
    resize(scores, to);
    typedef typename Iterator<TSegmentMatches, Standard>::Type TMatchIter;
    typedef typename Iterator<TScoreValues, Standard>::Type TScoreIter;
    TScoreIter itScore = begin(scores, Standard());
    TScoreIter itScoreEnd = end(scores, Standard());
    TMatchIter itMatch = begin(matches, Standard());
    itScore+=from;
    itMatch+=from;
    for(;itScore != itScoreEnd; ++itScore, ++itMatch) *itScore = (*itMatch).len;
  }
}

template<typename TString,
	 typename TSpec,
	 typename TScoreType,
	 typename TSize,
	 typename TSpec2,
	 typename TScoreString,
	 typename TScoreValue,
	 typename TWeightString>
inline void
rescoreMatches_sm(StringSet<TString, TSpec> const& str,
		  TScoreType const& scType,
		  String<Fragment<TSize, ExactFragment<> >, TSpec2> const& matches,
		  TScoreString& scores,
		  TScoreValue offset,
		  TWeightString& weights){
  //SEQAN_CHECKPOINT
  typedef String<Fragment<TSize, ExactFragment<> >, TSpec2> TFragmentString;
  typedef typename Id<typename Value<TFragmentString>::Type>::Type TId;
  typedef typename Iterator<TFragmentString, Standard>::Type TFragmentStringIter;
  typedef typename Iterator<TString, Standard>::Type TStringIter;
  typedef typename Iterator<TScoreString, Standard>::Type TScoreStringIter;
  typedef typename Iterator<TWeightString, Standard>::Type TWeightStringIter;
  resize(scores, length(matches));
  resize(weights, length(matches));

  // Get the scores
  TFragmentStringIter itF = begin(matches, Standard() );
  TFragmentStringIter itFEnd = end(matches, Standard() );
  TScoreStringIter itSc = begin(scores, Standard() );
  TWeightStringIter itW = begin(weights, Standard());
  TId id1 = 0;
  TId id2 = 0;
  TSize pos1 = 0;
  TSize pos2 = 0;
  TSize fragLen = 0;
  for(; itF != itFEnd; ++itF, ++itSc, ++itW) {
    id1 = sequenceId(*itF,0);
    id2 = sequenceId(*itF,1);
    pos1 = fragmentBegin(*itF, id1);
    pos2 = fragmentBegin(*itF, id2);
    fragLen = fragmentLength(*itF, id1);
    TStringIter itS1 = begin(str[idToPosition(str, id1)], Standard() );
    itS1 += pos1;
    TStringIter itS2 = begin(str[idToPosition(str, id2)], Standard() );
    itS2 += pos2;
    *itSc = 0;
    for(TSize i = 0; i<fragLen; ++i, ++itS1, ++itS2)
      *itSc += offset + score(scType, *itS1, *itS2);
    *itSc= (*itSc)* (*itW);
  }
}


template<typename TString,
	 typename TSpec1,
	 typename TValue,
	 typename TSpec2,
	 typename TSize,
	 typename TSpec3,
	 typename TScoreString,
	 typename TWeightString>
inline void
rescoreMatches_identity(StringSet<TString, TSpec1> const& str,
			String<TValue, TSpec2> const& sim,
			String<Fragment<TSize, ExactFragment<> >, TSpec3> const& matches,
			TScoreString& scores,
			TWeightString& weights){

  //SEQAN_CHECKPOINT
  typedef String<Fragment<TSize, ExactFragment<> >, TSpec3> TFragmentString;
  typedef typename Id<typename Value<TFragmentString>::Type>::Type TId;
  typedef typename Iterator<TFragmentString, Standard>::Type TFragmentStringIter;
  typedef typename Iterator<TString, Standard>::Type TStringIter;
  typedef typename Iterator<TScoreString, Standard>::Type TScoreStringIter;
  typedef typename Iterator<TWeightString, Standard>::Type TWeightStringIter;
  typedef typename Position<String<TValue,TSpec2> >::Type TSize2;
  resize(scores, length(matches));

  // Get the scores
  TFragmentStringIter itF = begin(matches, Standard() );
  TFragmentStringIter itFEnd = end(matches, Standard() );
  TScoreStringIter itSc = begin(scores, Standard() );
  TWeightStringIter itW = begin(weights, Standard());
  TId id1 = 0; TId id2 = 0;
  TSize fragLen=0;
  TSize2 simi1;
  TSize2 simi2;
  TSize2 nseq= (TSize2) length(str);

  //TSize pos1 = 0; TSize pos2 = 0;TSize fragLen = 0;
  for(; itF != itFEnd; ++itF, ++itSc, ++itW) {
    id1 = sequenceId(*itF,0);
    id2 = sequenceId(*itF,1);
    simi1= (TSize2)idToPosition(str,id1);
    simi2= (TSize2)idToPosition(str,id2);
    fragLen = fragmentLength(*itF, id1);
    if(simi1 < simi2)
      *itSc = ((double)fragLen) * sim[simi1*nseq + simi2];
    else
      *itSc = ((double)fragLen) * sim[simi2*nseq + simi1];
    *itSc = (*itSc) * (*itW);
  }
}

template<typename TSequence,
	 typename TNames,
	 typename TScore>
inline void
runMSA(Graph<Alignment<StringSet<TSequence, Dependent<> >, void, WithoutEdgeId> >& gAlign,
       StringSet<TSequence, Owner<> >& sequenceSet,
       TNames& sequenceNames,
       MsaParams const& msaParams,
       TScore const& score_type,
       bool DEBUG=false){

  typedef StringSet<TSequence, Owner<> > TStringSet;
  typedef typename Size<TStringSet>::Type TSize;
  clock_t t1,t2,t_total1,t_total2;
  t_total1= clock();

  // Initialize alignment object
  clear(gAlign);
  assignStringSet(gAlign, sequenceSet);
  StringSet<TSequence, Dependent<> >& str = stringSet(gAlign);

  // Some alignment constants
  TSize nSeq = length(str);
  TSize threshold = 30;

  // Select all possible pairs for global and local alignments
  String<TSize> pList;
  selectPairs(str, pList); // this works, checked

  // Set-up a distance matrix
  typedef double TDistanceValue;
  typedef String<TDistanceValue> TDistanceMatrix;
  TDistanceMatrix distanceMatrix;
  typedef String<double> TSimilarityMatrix;
  TSimilarityMatrix similarityMatrix;

  // Containers for segment matches, corresponding scores and infolabels
  typedef String<Fragment<> > TFragmentString;
  typedef Size<TFragmentString>::Type TFragmentStringSize;
  TFragmentString matches;
  typedef double TScoreValue;
  typedef String<TScoreValue> TScoreValues;
  TScoreValues scores;
  typedef double TWeightValue;
  typedef String<TWeightValue> TWeightValues;
  TWeightValues weights;

	// Include segment matches from alignment files
	if (!empty(msaParams.alifiles)) {
		if(msaParams.verbal){
			t1= clock();
			std::cout<<"parsing alignment files..\n";
		}
		TSize matchesFromIndex= length(matches);
		typedef typename Iterator<String<std::string>, Standard>::Type TIter;
		TIter begIt = begin(msaParams.alifiles, Standard() );
		TIter begItEnd = end(msaParams.alifiles, Standard() );
		for(;begIt != begItEnd; goNext(begIt)) {
			std::ifstream strm_lib;
			strm_lib.open((*begIt).c_str(), ::std::ios_base::in | ::std::ios_base::binary);
			if(strm_lib.fail()){
				std::cerr << "runMSA(): failed to open file "<<(*begIt)<<": exiting\n";
				exit(1);
			}
			read(strm_lib, matches, scores, sequenceNames, FastaAlign());
			strm_lib.close();
		}
		// labeling matches
		resize(weights,length(matches),msaParams.infoWeight[INFO_SRC_ALIGNMENT]);
		if(msaParams.verbal){
			t2= clock();
			std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		}
		if(DEBUG){
			std::cout<< "# DEBUG segment matches from alignment files:\n";
			printSegmentMatches(str,matches,scores,matchesFromIndex,length(matches));
		}
	}

	// Include a T-Coffee library
	if (!empty(msaParams.libfiles)) {
		if(msaParams.verbal){
			t1= clock();
			std::cout<<"parsing t-coffee library..\n";
		}
		TSize matchesFromIndex= length(matches);
		for(unsigned int i=0; i<length(msaParams.libfiles); i++){
			if(msaParams.verbal)
				std::cout<<"\treading "<<(msaParams.libfiles[i])<<"..\n";
			std::ifstream strm_lib;
			strm_lib.open(msaParams.libfiles[i].c_str(), std::ios_base::in | std::ios_base::binary);
			if(strm_lib.fail()){
				std::cerr << "runMSA(): failed to open file "<<(msaParams.libfiles[i])<<": exiting\n";
				exit(1);
			}
			read(strm_lib, matches, scores, sequenceNames, TCoffeeLib());
			strm_lib.close();
		}
		// labeling matches
		resize(weights,length(matches),msaParams.infoWeight[INFO_SRC_LIBRARY]);
		if(msaParams.verbal){
			t2= clock();
			std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		}
		if(DEBUG){
			std::cout<< "# DEBUG segment matches from libfiles\n";
			printSegmentMatches(str,matches,scores,matchesFromIndex,length(matches));
		}
	}

	// Include computed segment matches
	if(!empty(msaParams.method)) {
		if(msaParams.verbal){
			t1= clock();
			std::cout<<"computing segment matches..\n";
		}
		TSize matchesFromIndex;
		for(unsigned int i=0; i<length(msaParams.method); i++){
			if(msaParams.method[i]=="global"){
				matchesFromIndex= length(matches);
				appendSegmentMatches_gotoh(str, pList, score_type, matches, scores, distanceMatrix,similarityMatrix);
				resize(weights,length(matches),msaParams.infoWeight[INFO_SRC_GLOBAL]);
				if(DEBUG){
					std::cout<< "# DEBUG segment matches from global ali\n";
					printSegmentMatches(str,matches,scores,matchesFromIndex,length(matches));
				}
			}
			else if (msaParams.method[i]=="local"){
				matchesFromIndex= length(matches);
				appendSegmentMatches_swclump(str, pList, score_type, matches, scores, msaParams.nlocal, msaParams.plocal);
				resize(weights,length(matches),msaParams.infoWeight[INFO_SRC_LOCAL]);
				if(DEBUG){
					std::cout<< "# DEBUG segment matches from local ali\n";
					printSegmentMatches(str,matches,scores,matchesFromIndex,length(matches));
				}
			}
			else if (msaParams.method[i]=="lcs"){
				matchesFromIndex= length(matches);
				appendSegmentMatches_lcs(str, pList, score_type, matches, scores);
				resize(weights,length(matches),msaParams.infoWeight[INFO_SRC_LCS]);
				if(DEBUG){
					std::cout<< "# DEBUG segment matches from lcs ali\n";
					printSegmentMatches(str,matches,scores,matchesFromIndex,length(matches));
				}
			}
		}
		if(msaParams.verbal){
			t2= clock();
			std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		}
	}

	if(DEBUG){
		std::cout << "# DEBUG distance matrix:\n";
		if(empty(distanceMatrix))
			std::cout << "empty\n";
		else
			print(distanceMatrix,nSeq,nSeq);
		std::cout << "# DEBUG similarity matrix:\n";
		if(empty(similarityMatrix))
			std::cout << "empty\n";
		else
			print(similarityMatrix,nSeq,nSeq);
	}

  // Include MUMmer segment matches
  /*
  if (!empty(msaParams.mummerfiles)) {
    typedef typename Iterator<String<std::string>, Standard>::Type TIter;
    TIter begIt = begin(msaParams.mummerfiles, Standard() );
    TIter begItEnd = end(msaParams.mummerfiles, Standard() );
    for(;begIt != begItEnd; goNext(begIt)) {
      std::ifstream strm_lib;
      strm_lib.open((*begIt).c_str(), std::ios_base::in | std::ios_base::binary);
      read(strm_lib, matches, scores, str, sequenceNames, MummerLib());
      strm_lib.close();
    }
  }
  */

	// Use these segment matches for the initial alignment graph
	if(msaParams.verbal){
		t1= clock();
		std::cout<<"weighting segment matches..\n";
	}
	if(msaParams.weight == "sm"){
		rescoreMatches_sm(str,
				score_type,
				matches,
				scores,
				msaParams.smoffset,
				weights);
		if(DEBUG){
			std::cout << "# rescoreMatches_sm:\n";
			printSegmentMatches(str,matches,scores,(TSize)0,length(matches));
		}
	}
	else{
		rescoreMatches_identity(str,
					similarityMatrix,
					matches,
					scores,
					weights);
		if(DEBUG){
			std::cout << "# rescoreMatches_identity:\n";
			printSegmentMatches(str,matches,scores,(TSize)0,length(matches));
		}

	}
	if(msaParams.verbal){
		t2= clock();
		std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
	}

	// Segment-match refinement
	if(msaParams.verbal){
		t1= clock();
		std::cout<<"refine segment matches..\n";
	}
	typedef Graph<Alignment<StringSet<TSequence,Dependent<> >, double> > TRefineGraph;
	typedef typename Iterator<TRefineGraph, EdgeIterator>::Type TEdgeIterator;
	typedef typename Iterator<TFragmentString,Standard>::Type TFragmentStringIter;
	typedef typename Iterator<TScoreValues,Standard>::Type TScoreValuesIter;
	typedef typename Id<TRefineGraph>::Type TId;
	typedef typename EdgeDescriptor<TRefineGraph>::Type TEdgeDescriptor;
	typedef typename VertexDescriptor<TRefineGraph>::Type TVertexDescriptor;

	TRefineGraph gRefine(str);
	matchRefinement(matches,str,gRefine);
	// Clear edge-weights
	TEdgeIterator itE(gRefine);
	for(;!atEnd(itE);goNext(itE)) cargo(value(itE)) = 0;
	// Adapt the scores
	TFragmentStringIter it = begin(matches, Standard() );
	TFragmentStringIter endIt = end(matches, Standard() );
	TScoreValuesIter scoreIt = begin(scores, Standard() );
	for(; it != endIt; ++it, ++scoreIt) {
		TId id1 = sequenceId(*it,0);
		TId id2 = sequenceId(*it,1);
		TSize pos1 = fragmentBegin(*it, id1);
		TSize pos2 = fragmentBegin(*it, id2);
		TSize fragLen = fragmentLength(*it, id1);
		TSize end1 = pos1 + fragLen;
		while(pos1 < end1) {
			TVertexDescriptor p1 = findVertex(gRefine, id1, pos1);
			TVertexDescriptor p2 = findVertex(gRefine, id2, pos2);
			TSize vertexLen = fragmentLength(gRefine, p1);
			cargo(findEdge(gRefine, p1, p2)) += ((vertexLen * (*scoreIt)) / fragLen);
			pos1 += vertexLen;
			pos2 += vertexLen;
		}
	}
	clear(matches);
	clear(scores);
	clear(similarityMatrix);
	if(msaParams.verbal){
		t2= clock();
		std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
	}
	if(DEBUG){
		std::cout<<"# DEBUG refined graph:\n";
		printEdges(gRefine);
	}

	// Guide tree
	if(DEBUG){
		std::cout<< "# DEBUG building guide tree\n";
	}
	Graph<Tree<TDistanceValue> > guideTree;
	// Check if we have a valid distance matrix
	if (empty(distanceMatrix)) getDistanceMatrix(gRefine, distanceMatrix, KmerDistance());
	if (msaParams.build == "nj") njTree(distanceMatrix, guideTree);
	else if (msaParams.build == "min") upgmaTree(distanceMatrix, guideTree, UpgmaMin());
	else if (msaParams.build == "max") upgmaTree(distanceMatrix, guideTree, UpgmaMax());
	else if (msaParams.build == "avg") upgmaTree(distanceMatrix, guideTree, UpgmaAvg());
	else if (msaParams.build == "wavg") upgmaTree(distanceMatrix, guideTree, UpgmaWeightAvg());
	clear(distanceMatrix);



	// consistency transformations
	if(msaParams.consistent.compare("triplet1")==0){
		if(msaParams.verbal){
			t1=clock();
			cout<<"tripletLibraryExtension1..\n";
			cout.flush();
		}
		for(unsigned int i=0; i<msaParams.iconsistent; i++){
			if(msaParams.verbal)
				cout <<(i+1)<<". iteration..\n";
			tripletLibraryExtension1(gRefine);
		}
		if(msaParams.verbal){
			t2= clock();
			std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		}
		if(DEBUG){
			std::cout << "# DEBUG graph after tripletLibraryExtension1:\n";
			printEdges(gRefine);
		}
	}
	else if(msaParams.consistent.compare("triplet2")==0){
		if(msaParams.verbal){
			t1=clock();
			cout<<"tripletLibraryExtension2..\n";
			cout.flush();
		}
		for(unsigned int i=0; i<msaParams.iconsistent; i++){
			if(msaParams.verbal)
				cout <<(i+1)<<". iteration..\n";
			tripletLibraryExtension2_3(gRefine, guideTree, threshold / 2);
		}
		if(msaParams.verbal){
			t2= clock();
			std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		}
		if(DEBUG){
			std::cout << "# DEBUG graph after tripletLibraryExtension2:\n";
			printEdges(gRefine);
		}
	}
	else if(msaParams.consistent.compare("maxflow")==0){
		if(msaParams.verbal){
			t1=clock();
			cout<<"maxflowConsistentcy..\n";
			cout.flush();
		}
		for(unsigned int i=0; i<msaParams.iconsistent; i++){
			if(msaParams.verbal)
				cout <<(i+1)<<". iteration..\n";
			//maxflowConsistency_simle(gRefine);
			maxflowConsistency_fast(gRefine);
		}
		if(msaParams.verbal){
			t2= clock();
			std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		}
		if(DEBUG){
			std::cout << "# DEBUG graph after maxflowConsistency:\n";
			printEdges(gRefine);
		}
	}


	// clique transformation
	if(msaParams.cliquetr){
		if(msaParams.verbal){
			t1=clock();
			cout<<"cliqueTransformation..\n";
			cout.flush();
		}
		cliqueTransformation_simple(gRefine,true);
		if(msaParams.verbal){
			t2=clock();
			std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		}
		if(DEBUG){
			std::cout << "# DEBUG graph after clique transformation:\n";
			printEdges(gRefine);
		}
	}

	// Progressive Alignment
	typedef String<String<TVertexDescriptor> > TSegmentString;
	TSegmentString profileOut;
	if(msaParams.verbal){
		t1=clock();
		cout<<"progressive alignment..\n";
		cout.flush();
	}
	progressiveAlignment_main(gRefine, guideTree, profileOut);
	if(msaParams.verbal){
		t2=clock();
		std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
	}

	// iterative refinement
	if(msaParams.refine!= "none"){
		if(msaParams.verbal){
			t1=clock();
			cout<<"iterative refinement..\n";
			cout.flush();
		}
		if(msaParams.refine=="random")
			iterativeRefinement_random(gRefine, profileOut, msaParams.irefine);
		else if(msaParams.refine== "tree1") // TODO
			iterativeRefinement_tree1(gRefine, guideTree, profileOut, msaParams.irefine);
		else if(msaParams.refine== "tree2")
			iterativeRefinement_tree2(gRefine, guideTree, profileOut);
		if(msaParams.verbal){
			t2=clock();
			std::cout<<"done in "<< (t2-t1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		}
	}

	// Create the alignment graph
	_createAlignmentGraph(gRefine, profileOut, gAlign);
	clear(guideTree);
	clear(gRefine);

	if(msaParams.verbal){
		t_total2= clock();
		std::cout<<"MSA done in "<< (t_total2-t_total1)<<"/"<<double(CLOCKS_PER_SEC)<<"="<<((t_total2-t_total1)/double(CLOCKS_PER_SEC))<<" sec\n";
	}
}


#endif
