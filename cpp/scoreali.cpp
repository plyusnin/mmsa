#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <seqan/misc/misc_cmdparser.h>
#include <seqan/graph_msa.h> // seqan::tcoffee libraries
#include <seqan/sequence.h>
#include <seqan/refinement.h>
#include <seqan/score.h>
using namespace seqan;
using namespace std;
/*
 * Prog for calculating sum of paris (SP) score for a given alignment with a given scoring scheem.
 */

template<typename TSeqString,
		typename TSeqSpec,
		typename TNameString,
		typename TNameSpec>
void printSequences(StringSet<TSeqString,TSeqSpec >& seqSet,
					StringSet<TNameString,TNameSpec >& nameSet){
	typedef typename Iterator<StringSet<TSeqString,TSeqSpec > >::Type Tseqit;
	typedef typename Iterator<StringSet<TNameString,TNameSpec > >::Type Tnameit;
	Tseqit seqit= begin(seqSet);
	Tnameit nameit= begin(nameSet);
	for(;seqit!=end(seqSet) && nameit!=end(nameSet); ++seqit,++nameit){
		cout<<">"<<(*nameit)<<"\n"
			<<(*seqit)<<"\n";
	}
}

template<typename TString,
	 typename TSpec,
	 typename TSegmentMatches,
	 typename TScores,
	 typename TSize>
void printSegmentMatches(StringSet<TString,TSpec> const& str,
			 TSegmentMatches const& matches,
			 TScores const& scores,
			 TSize from,
			 TSize to){
      typedef typename Iterator<TSegmentMatches, Standard>::Type TSegmentMatchIter;
      typedef typename Iterator<TScores, Standard>::Type TScoreIter;
      typedef StringSet<TString,TSpec> TStringSet;
      typedef typename Id<typename Value<TSegmentMatches>::Type>::Type TId;

      typename Infix<TString >::Type qinf;
      typename Infix<TString >::Type sinf;
      TSegmentMatchIter itMatch= begin(matches);
      TSegmentMatchIter itMatchEnd= begin(matches);
      TScoreIter itScore = begin(scores, Standard());
      TScoreIter itScoreEnd = begin(scores, Standard());
      itMatch+= from;
      itMatchEnd+= to;
      itScore+= from;
      itScoreEnd+= to;
      if(itMatch != itMatchEnd){
	std::cout << "# matches for "<< (itMatch->seqId1) <<" x "<<(itMatch->seqId2)<<"\n";
      }

      unsigned int q1,q2,s1,s2;

      for(unsigned int i=1;itMatch != itMatchEnd && itScore != itScoreEnd; i++,itMatch++,itScore++){
	q1= itMatch->begin1;
	q2= (itMatch->begin1)+(itMatch->len);
	s1= itMatch->begin2;
	s2= (itMatch->begin2)+(itMatch->len);
	std::cout << "qid="<<(itMatch->seqId1)<<" sid="<<(itMatch->seqId2)<<" score="<<(*itScore)<<"\n"
		  << "qseq["<<q1<<","<<q2<<"[="<< infix(str[idToPosition(str,itMatch->seqId1)],q1,q2)<<"\n"
		  << "sseq["<<s1<<","<<s2<<"[="<< infix(str[idToPosition(str,itMatch->seqId2)],s1,s2)<<"\n";
      }
}


/*
 * Parces multiple fasta alignment to seqan data structures passed as references:
 * Sequences are saved to seqSet,
 * sequence names to nameSet,
 * refined fragment matches to aligraph.
 * Note that aligraph does not save the sequences, it only saves information on
 * which fragments of sequence in seqSet form vertices of the alignment graph, and
 * which vertices are connected by edges. Alignment graph is of Dependent<> type
 * and will perform unpredictedly if seqSet is to be delated. Note also that Owner<>
 * Alignment Graph is not implemented in seqan 1.3.
 * Check the returned graph using numEdges(aligraph)>0 numVertices(aligraph)>0
 */
template<typename TSeqString,
	 typename TNameString,
	 typename TCargo>
inline void
readMFA(const char* mfa_file,
		Graph<Alignment<StringSet<TSeqString, Dependent<> >, TCargo> >& aligraph,
		StringSet<TSeqString, Owner<> >& seqSet,
		StringSet<TNameString, Owner<> >& nameSet){

	typedef String<Fragment<> > TFragmentString;
	typedef Size<TFragmentString >::Type TFragmentStringSize;
	typedef String<TCargo> TScoreString;
	TFragmentString matches;
	TScoreString scores;

	//clearing containers
	clear(aligraph);
	clear(seqSet);
	clear(nameSet);

	// read sequences using graph_align_tcoffee_io.h 333
	std::ifstream strm_fasta;
	//cout << "readMFA::reading seqs from "<<mfa_file<<"\n";
	strm_fasta.open(mfa_file, ::std::ios_base::in | ::std::ios_base::binary);
	read(strm_fasta,seqSet,nameSet,FastaAlign());
	strm_fasta.close();
	//printSequences(seqSet,nameSet);

	// read pairwise Fragment matches using graph_align_tcoffee_io.h 436
	std::ifstream strm_ali;
	//cout << "readMFA::reading matches from "<<mfa_file<<"\n";
	strm_ali.open(mfa_file, ::std::ios_base::in | ::std::ios_base::binary);
	read(strm_ali, matches, scores, nameSet, FastaAlign());
	strm_ali.close();
	//printSegmentMatches(seqSet,matches,scores,(TFragmentStringSize)0,length(matches));


	//typedef Graph<Alignment<StringSet<TSeqString,Dependent<> >,TCargo > > TGraph;
	//TGraph aligraph;
	assignStringSet(aligraph,seqSet);
	matchRefinement(matches,seqSet,aligraph);
	//cout << "readMFA::aligraph after refinement:\n"<<aligraph;
}


int main(int argc, const char *argv[]) {
	CommandLineParser parser;
	addTitleLine(parser, "Calculates sum of pairs score for a given alignment using scoring matrix, gap extension and gap open scores. Prints a single line of output: ali_file\\tsc_matrix\\tgex\\tgop\\tsp_score\\n");
	addTitleLine(parser, " (c) Copyright 2010 by Pljusnin Ilja");
	addTitleLine(parser, " email: ilja.pljusnin@gmail.com");
	addUsageLine(parser, "-a <FASTA alignment file> -g <gap open penalty> -e <gap extension penalty> -ma <Blosum30|Blosum45|Blosum62|Blosum80> [-v]");
	addOption(parser, addArgumentText(CommandLineOption("a", "ali", "FASTA alignment file", (OptionType::String+OptionType::Mandatory)), "<FASTA Sequence File>"));
	addOption(parser, addArgumentText(CommandLineOption("g", "gop", "gap open penalty", (int)OptionType::Int, -13), "<Int>"));
	addOption(parser, addArgumentText(CommandLineOption("e", "gex", "gap extension penalty", (int)OptionType::Int, -1), "<Int>"));
	addOption(parser, addArgumentText(CommandLineOption("ma", "matrix", "Blosum30,Blosum45,Blosum62 or Blosum80", (int)OptionType::String, "Blosum62"), "<matrix type>"));
  	addOption(parser, CommandLineOption("v", "verbal", "prints input parameters", OptionType::Boolean,false));
	if (argc == 1){
		shortHelp(parser, std::cerr);// print short help and exit
		return 0;
	}
	if (!parse(parser, argc, argv, ::std::cerr)) return 1;
	if (isSetLong(parser, "help")){
		help(parser,std::cerr); // print long help and exit
		return 0;
	}

	std::string ali_file="none";
	int gop=-13;
	int gex= -1;
	std::string matrix="Blosum62";
	if(!getOptionValueLong(parser,"ali",ali_file) || ali_file=="none"){
		std::cerr <<"no alignment specified: exiting\n";
		exit(1);
	}
	getOptionValueLong(parser,"gop",gop);
	getOptionValueLong(parser,"gex",gex);
	getOptionValueLong(parser,"matrix",matrix);

	typedef String<AminoAcid> TSeqString;
	typedef String<char> TNameString;
	typedef StringSet<TSeqString, Owner<> > TSeqStringSet;
	typedef StringSet<TNameString, Owner<> > TNameStringSet;
	typedef double TCargo;
	typedef Graph<Alignment<StringSet<TSeqString,Dependent<> >,TCargo > > TGraph;
	TSeqStringSet alignment_sequences;
	TNameStringSet alignment_seqnames;
	TGraph alignment_graph;

	readMFA(ali_file.c_str(),alignment_graph,alignment_sequences,alignment_seqnames);
	if( numVertices(alignment_graph)==0 || numEdges(alignment_graph)==0){
		printf("problems parsing file=%s:exiting\n",ali_file.c_str());
		exit(1);
	}
	//DEBUG
	//cout<<"sequences:\n";
	//printSequences(alignment_sequences,alignment_seqnames);
	if(isSetLong(parser,"verbal")){
		cout<<"AlignmentGraph:\n"<<alignment_graph<<"\n";
	}
	//DEBUG

	//score alis
	double sp_scores;
	if(matrix == "Blosum30"){
		Blosum30 score(gex,gop);
		sp_scores= sumOfPairsScore(alignment_graph,score);
	}
	else if(matrix == "Blosum45"){
		Blosum45 score(gex,gop);
		sp_scores= sumOfPairsScore(alignment_graph,score);
	}
	else if(matrix == "Blosum80"){
		Blosum80 score(gex,gop);
		sp_scores= sumOfPairsScore(alignment_graph,score);
	}
	else if(matrix == "Blosum62"){ // default
		Blosum62 score(gex,gop);
		sp_scores= sumOfPairsScore(alignment_graph,score);
	}
	printf("%s\t%s\t%d\t%d\t%.3f\n",ali_file.c_str(),matrix.c_str(),gex,gop,sp_scores);
}
