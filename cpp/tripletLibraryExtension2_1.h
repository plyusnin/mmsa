#ifndef TRIPLET_LIBRARY_EXTENSION2_1_H
#define TRIPLET_LIBRARY_EXTENSION2_1_H
#include <time.h>

/*
 * Triplet library extension implemented using merge sorting and sparce matrices.
 * Implementation is similar to sparce matrix multiplication.
 *
 * This approach scales much better then seqan::graph_msa::graph_align_tcoffee_base.h::tripletLibraryExtension(..)
 * functions relative to the number of edges. Comparison based on BaliBase BB11005.tfa:
 *
 * # iterations		A		B
 * 1				0.53	1.26
 * 2				17		4
 * 3				~2500	161
 * where A is seqan::graph_msa::graph_align_tcoffee_base.h::tripletLibraryExtension(aliGraph,guideTree,minMembers=15)
 * and B tripletLibraryExtension2_1(aliGraph).
 */
template<typename TStringSet, typename TCargo, typename TSpec>
inline void
tripletLibraryExtension2_1(Graph<Alignment<TStringSet, TCargo, TSpec> >& g)
{
	SEQAN_CHECKPOINT
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename Size<TGraph>::Type TSize;
	typedef typename VertexDescriptor<TGraph>::Type TVertexDescriptor;
	typedef typename Iterator<TGraph, VertexIterator>::Type TVertexIterator;
	typedef typename Iterator<TGraph, EdgeIterator>::Type TEdgeIterator;
	//DEBUG
	//clock_t t1,t2;

	// data structures
	TStringSet seqs= getStringSet(g);
	unsigned int Nseqs= length(seqs);
	unsigned int Nedges= numEdges(g);
	unsigned int Nvert= numVertices(g);
	vector<vector<vector<pair<TCargo,unsigned int> > > > XY(Nseqs*Nseqs); // adjacency matrices for each pair of sequences. Stores values primarily by row. The main diagonal stores edges from each vertex to itself, with a constant edge weight equal to unity. Uninty edges from vertex to itself eliminate the need for special treatment when multiplying XX x XY.
	vector<vector<vector<pair<TCargo,unsigned int> > > > XYprime(Nseqs*Nseqs); // result of matrix multiplication
	vector<vector<TVertexDescriptor> > pos2vertex(Nseqs,vector<TVertexDescriptor>() );
	vector<std::map<unsigned int,TVertexDescriptor> > vertex2pos(Nseqs, std::map<unsigned int,TVertexDescriptor>() );


	// constructing pos2vertex and vertex2pos maps
	//cout<<"building pos2vertex and vertex2pos..\n";
	//t1=clock();
	TVertexIterator itV(g);
	for(;!atEnd(itV);goNext(itV)){
		unsigned int seqId= sequenceId(g,*itV);
		pos2vertex[seqId].push_back(*itV);
	}
		// sorting vertices of each sequence by position
	for(unsigned int i=0; i<Nseqs; i++){
		std::sort(pos2vertex[i].begin(),pos2vertex[i].end()); // using < operator
	}
		// now pos2vertex stands up for its name, we can construct vertex2pos
	for(unsigned int i=0; i<Nseqs; i++){
		for(unsigned int j=0; j<pos2vertex[i].size(); j++){
			vertex2pos[i][pos2vertex[i][j]]= j;
		}
	}
	//t2=clock();
	//cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";


	/* DEBUG
	cout<< "pos\tpos2vertex[pos]\tvertex2pos[pos2vertex]\n";
	for(unsigned int i=0; i<Nseqs; i++){
		cout<<(i+1)<<": "<<seqs[i]<<"\n";
		for(int k=0; k<pos2vertex[i].size(); k++){
			cout<<k<<"\t"<<label(g,pos2vertex[i][k])<<"\t"<<vertex2pos[i][pos2vertex[i][k]]<<"\n";
		}
	}*/


	// initiating sparce matrices
	for(unsigned int i=0; i<Nseqs; i++){
		for(unsigned int j=0; j<Nseqs; j++){
			XY[Nseqs*j+i].resize(pos2vertex[i].size(), vector<pair<TCargo,unsigned int> >() );
		}
	}
	for(unsigned int i=0; i<Nseqs; i++){
		for(unsigned int j=0; j<Nseqs; j++){
			XYprime[Nseqs*j+i].resize(pos2vertex[i].size(), vector<pair<TCargo,unsigned int> >() );
		}
	}


	// populating sparce matrices
	//cout<<"populating sparce matrices..\n";
	//t1=clock();
		// upper right triangle
	TEdgeIterator itE(g);
	for(;!atEnd(itE);goNext(itE)) {
		TVertexDescriptor sV = sourceVertex(itE);
		TVertexDescriptor tV = targetVertex(itE);
		TCargo c = cargo(*itE);
		unsigned int sSeq= sequenceId(g,sV);
		unsigned int tSeq= sequenceId(g,tV);
		unsigned int sPos= vertex2pos[sSeq][sV];
		unsigned int tPos= vertex2pos[tSeq][tV];
		if(sSeq < tSeq){ // upper right triangle
			XY[Nseqs*tSeq+sSeq][sPos].push_back(pair<TCargo,unsigned int>(c,tPos) );
		}
		else{ // lower left triangle
			XY[Nseqs*sSeq+tSeq][tPos].push_back(pair<TCargo,unsigned int>(c,sPos) );
		}
	}
		// lower left triangle
	for(unsigned int i=0; i<Nseqs; i++){
		for(unsigned int j=i+1; j<Nseqs; j++){
			unsigned int uind= Nseqs*j+i;
			for(unsigned int xpos=0; xpos<XY[uind].size(); xpos++){
				for(unsigned int k=0; k<XY[uind][xpos].size(); k++){
					TCargo c= XY[uind][xpos][k].first;
					unsigned int ypos= XY[uind][xpos][k].second;
					unsigned int lind= Nseqs*i+j;
					XY[lind][ypos].push_back(pair<TCargo,unsigned int>(c,xpos));
				}
			}
		}
	}
		// clearing upper right triangle and repopulating with edges from lower left triangle, this is equivalent to sorting edges in the upper triangle by target position
	for(unsigned int i=0; i<Nseqs; i++){
		for(unsigned int j=i+1; j<Nseqs; j++){
			unsigned int uind= Nseqs*j+i;
			for(unsigned int xpos=0; xpos<XY[uind].size(); xpos++){
				XY[uind][xpos].clear();
			}
		}
	}
	for(unsigned int i=0; i<Nseqs; i++){
		for(unsigned int j=0; j<i; j++){
			unsigned int lind= Nseqs*j+i;
			for(unsigned int ypos=0; ypos<XY[lind].size(); ypos++){
				for(unsigned int k=0; k<XY[lind][ypos].size(); k++){
					TCargo c= XY[lind][ypos][k].first;
					unsigned int xpos= XY[lind][ypos][k].second;
					unsigned int uind= Nseqs*i+j;
					XY[uind][xpos].push_back(pair<TCargo,unsigned int>(c,ypos) );
				}
			}
		}
	}
		// main diagonal
	for(unsigned int i=0; i<Nseqs; i++){
		unsigned int ind= Nseqs*i+i;
		for(unsigned int xpos=0; xpos<XY[ind].size(); xpos++){
			XY[ind][xpos].push_back(pair<TCargo,unsigned int>(1,xpos) ); // unity edge to itself
		}
	}
	//t2=clock();
	//cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";


	/* DEBUG
	cout<<"XY:\n";
	for(unsigned int i=0; i<Nseqs; i++){
		cout<<"X:"<<(i+1)<<":"<<seqs[i]<<"\n";
		for(unsigned int j=0; j<Nseqs; j++){
			cout<<"\tY:"<<(j+1)<<":"<<seqs[j]<<"\n";
			unsigned int ind= Nseqs*j+i;
			for(unsigned int xpos=0; xpos<XY[ind].size(); xpos++){
				vector<pair<TCargo,unsigned int> >& temp= XY[ind][xpos];
				cout<<"\t\tX:"<<xpos<<":"<<label(g,pos2vertex[i][xpos]);
				for(int k=0; k<temp.size(); k++)
					cout<<" Y:"<<temp[k].second<<":"<<label(g,pos2vertex[j][temp[k].second])<<":"<<temp[k].first;
				cout<<"\n";
			}
		}
	}*/


	// executing triplet extension based on merge sorting edge lists in sparce matrices
	typedef typename vector<vector<pair<TCargo,unsigned int> > >::iterator TSparceMatrixPositionIterator;
	typedef typename vector<pair<TCargo,unsigned int> >::iterator TTargetIterator;
	typedef typename vector<TCargo>::iterator TCargoIterator;
	TCargoIterator cargoIt;
	//cout<<"merge sort based triple extension..\n";
	//t1=clock();
	for(unsigned int xi=0; xi<Nseqs; xi++){ // iterating pairs of sequences
		for(unsigned int yi=0; yi<xi; yi++){
			//DEBUG
			//cout<< "X="<<xi<<" Y="<<yi<<"\n";
			vector<TCargo> cargo((pos2vertex[xi].size())*(pos2vertex[yi].size()),0); //cargo for each pair of XY positions

			for(unsigned int zi=0; zi<Nseqs; zi++){ // iterating third sequence to complite the triplet
				if(zi==xi || zi==yi) // XX x XY = XY or XY x YY = XY cases
					continue;
				//cout<< "\tZ="<<zi<<"\n";
				vector<vector<pair<TCargo,unsigned int> > >& XZref= XY[Nseqs*zi+xi];
				vector<vector<pair<TCargo,unsigned int> > >& YZref= XY[Nseqs*zi+yi];
				cargoIt= cargo.begin();

				for(unsigned int xp=0; xp<pos2vertex[xi].size(); xp++){ // iterating the pairs of profile positions
					vector<pair<TCargo,unsigned int> >& xz_target= XZref[xp];
					if(xz_target.size()==0){
						cargoIt+= pos2vertex[yi].size();
						continue;
					}
					for(unsigned int yp=0; yp<pos2vertex[yi].size(); yp++,cargoIt++){
						vector<pair<TCargo,unsigned int> >& yz_target= YZref[yp];
						if(yz_target.size()==0)
							continue;
						if(xz_target.back().second < yz_target.front().second
						|| xz_target.front().second > yz_target.back().second)
							continue; // shortcut when ranges do not overlap
						TTargetIterator i= xz_target.begin();
						TTargetIterator j= yz_target.begin();
						while(i!=xz_target.end() && j!=yz_target.end()){
							if(i->second < j->second){
								i++;
							}
							else if(i->second > j->second){
								j++;
							}
							else{ // we have a triplet x->z->y
								*cargoIt+= (i->first < j->first) ? i->first : j->first; // min(cargo(x,z),cargo(z,y))
								i++;
								j++;
							}
						}
					}
				}
			}

			// XY edge
			//cout<<"inserting XY edge..\n";
			vector<vector<pair<TCargo,unsigned int> > >& XYref= XY[Nseqs*yi+xi];
			cargoIt= cargo.begin();
			for(unsigned int xp=0; xp<pos2vertex[xi].size(); xp++){ // iterating the pairs of profile positions
				vector<pair<TCargo,unsigned int> >& xz_target= XYref[xp];
				for(unsigned int yp=0; yp<pos2vertex[yi].size(); yp++,cargoIt++){
					for(TTargetIterator i= xz_target.begin(); i!=xz_target.end(); i++){
						if(i->second == yp){
							*cargoIt+= i->first;
							break;
						}
					}
				}
			}

			/*DEBUG
			cout<<"cargo:\n";
			for(unsigned int yp=0; yp<pos2vertex[yi].size(); yp++)
				cout<<"\t"<<yp;
			cout<<"\n";
			cargoIt= cargo.begin();
			for(unsigned int xp=0; xp<pos2vertex[xi].size(); xp++){
				cout<<xp;
				for(unsigned int yp=0; yp<pos2vertex[yi].size(); yp++,cargoIt++)
					cout<<"\t"<<(*cargoIt);
				cout<<"\n";
			}*/

			// populating XYprime
			vector<vector<pair<TCargo,unsigned int> > >& XYprimeRef= XYprime[Nseqs*yi+xi];
			cargoIt= cargo.begin();
			for(unsigned int xp=0; xp<pos2vertex[xi].size(); xp++){
				for(unsigned int yp=0; yp<pos2vertex[yi].size(); yp++,cargoIt++){
					if(*cargoIt>0)
						XYprimeRef[xp].push_back(pair<TCargo,unsigned int>(*cargoIt,yp) );
				}
			}
		}
	}
	//t2=clock();
	//cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";

	/*DEBUG
	cout<<"XYprime:\n";
	for(unsigned int i=0; i<Nseqs; i++){
		cout<<"X:"<<(i+1)<<":"<<seqs[i]<<"\n";
		for(unsigned int j=0; j<Nseqs; j++){
			cout<<"\tY:"<<(j+1)<<":"<<seqs[j]<<"\n";
			unsigned int ind= Nseqs*j+i;
			for(unsigned int xpos=0; xpos<XYprime[ind].size(); xpos++){
				vector<pair<TCargo,unsigned int> >& temp= XYprime[ind][xpos];
				cout<<"\t\tX:"<<xpos<<":"<<label(g,pos2vertex[i][xpos]);
				for(int k=0; k<temp.size(); k++)
					cout<<" Y:"<<temp[k].second<<":"<<label(g,pos2vertex[j][temp[k].second])<<":"<<temp[k].first;
				cout<<"\n";
			}
		}
	}*/


	// copying collection of extended edges to the AlignmentGraph
	//cout<<"insert edges..\n";
	//t1=clock();
	clearEdges(g);
	for(unsigned int xi=0; xi<Nseqs; xi++){
		for(unsigned int yi=0; yi<xi; yi++){
			unsigned int xyi= Nseqs*yi+xi;
			for(unsigned int xp=0; xp<XYprime[xyi].size(); xp++){
				vector<pair<TCargo,unsigned int> >& xy_temp= XYprime[xyi][xp];
				for(unsigned int k=0; k<xy_temp.size(); k++){
					addEdge(g, pos2vertex[xi][xp], pos2vertex[yi][xy_temp[k].second], xy_temp[k].first);
				}
			}
		}
	}
	//t2=clock();
	//cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
}

#endif
