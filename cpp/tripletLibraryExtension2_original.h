#ifndef TRIPLET_LIBRARY_EXTENSION_ORIGINAL_H
#define TRIPLET_LIBRARY_EXTENSION_ORIGINAL_H
#include <time.h>

/*
 * Modified
 * seqan::graph_msa::graph_align_tcoffee_base.h::tripletLibraryExtension(aliGraph,guideTree,minMembers)
 * for execution timing and debuging.
 */
template<typename TStringSet, typename TCargo, typename TSpec, typename TGuideTree, typename TSize>
inline void
tripletLibraryExtension_original(Graph<Alignment<TStringSet, TCargo, TSpec> >& g,
						TGuideTree& guideTree,
						TSize minMembers)
{
	cout << "tripletLibraryExtension..\n";
	SEQAN_CHECKPOINT
	typedef Graph<Alignment<TStringSet, TCargo, TSpec> > TGraph;
	typedef typename VertexDescriptor<TGraph>::Type TVertexDescriptor;
	typedef typename Iterator<TGraph, EdgeIterator>::Type TEdgeIterator;
	typedef typename VertexDescriptor<TGuideTree>::Type TTreeVertex;
	TStringSet& strSet = stringSet(g);
	TSize nSeq = length(strSet);
	clock_t t1,t2;

	cout << "subtree search..\n";
	t1=clock();
	// Identify large subtrees
	typedef String<TSize> TSequenceGroups;
	String<TSequenceGroups> seqGroup;
	typedef String<TTreeVertex> TGroupRoot;
	TGroupRoot groupRoot;
	_subTreeSearch(guideTree, seqGroup, groupRoot, minMembers);
	t2=clock();
	cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";

	// Label the subtree sequences
	String<TSize> seqLabels;
	resize(seqLabels, nSeq);
	typedef typename Iterator<TSequenceGroups, Standard>::Type TSeqSetIter;
	for(TSize i=0; i< (TSize) length(seqGroup); ++i) {
		TSeqSetIter itSeqGroup = begin(seqGroup[i], Standard());
		TSeqSetIter itSeqGroupEnd = end(seqGroup[i], Standard());
		for(;itSeqGroup != itSeqGroupEnd; ++itSeqGroup)
			seqLabels[*itSeqGroup] = i;
	}

	// Store all edges
	cout << "Store all edges..\n";
	typedef std::pair<TVertexDescriptor, TVertexDescriptor> TNewEdge;
	typedef std::map<TNewEdge, TCargo> TEdgeMap;
	typedef typename TEdgeMap::iterator TEdgeMapIter;
	TEdgeMap newEMap;
	typedef MsaEdgeCargo_<TVertexDescriptor, TCargo> TEdge;
	typedef String<TEdge> TEdgeString;
	TEdgeString initialEdges;
	TEdgeIterator itE(g);
	for(;!atEnd(itE);goNext(itE)) {
		TVertexDescriptor sV = sourceVertex(itE);
		TVertexDescriptor tV = targetVertex(itE);
		TCargo c = cargo(*itE);
		appendValue(initialEdges, TEdge(sV, tV, c), Generous());
		newEMap.insert(std::make_pair(TNewEdge(sV, tV), c));
	}
	clearEdges(g);


	// Perform triplet extension
	cout << "Perform triplet extension..\n";
	typedef typename Iterator<TEdgeString, Standard>::Type TEdgeIter;
	TEdgeString fullEdges;
	for(TSize i=0; i< (TSize) length(seqGroup); ++i) {
		cout << "group "<<(i+1)<<", length="<<length(seqGroup[i])<<"\n";
		cout << "collecting edges..\n";
		t1= clock();
		TEdgeIter itInitial = begin(initialEdges, Standard());
		TEdgeIter itInitialEnd = end(initialEdges, Standard());
		for(; itInitial != itInitialEnd; ++itInitial) {
			TSize seq1 = idToPosition(strSet, sequenceId(g, (*itInitial).v1));
			TSize seq2 = idToPosition(strSet, sequenceId(g, (*itInitial).v2));
			if ((seqLabels[seq1] == i) && (seqLabels[seq2] == i)) {
				appendValue(fullEdges, *itInitial, Generous());
				appendValue(fullEdges, TEdge((*itInitial).v2, (*itInitial).v1, (*itInitial).c), Generous());
			}
		}
		t2= clock();
		cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";

		cout<< "sorting..\n";
		t1= clock();
		::std::sort(begin(fullEdges, Standard()), end(fullEdges, Standard()), LessMsaEdgeCargo_<TVertexDescriptor, TCargo>() );
		t2= clock();
		cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";

		cout<< "iterating triplets..\n";
		t1= clock();
		typedef typename Iterator<TEdgeString, Standard>::Type TEdgeIter;
		TEdgeIter itEdges1 = begin(fullEdges, Standard());
		TEdgeIter itEdgesEnd = end(fullEdges, Standard());
		for(; itEdges1 != itEdgesEnd; ++itEdges1) {
			for(TEdgeIter itEdges2 = itEdges1; ++itEdges2 != itEdgesEnd;) {
				if ((*itEdges1).v1 != (*itEdges2).v1) break;
				if (sequenceId(g, (*itEdges1).v2) != sequenceId(g, (*itEdges2).v2)) {
					TCargo weight = ((*itEdges1).c < (*itEdges2).c) ? (*itEdges1).c : (*itEdges2).c;

					if ((*itEdges1).v2 < (*itEdges2).v2) {
						TEdgeMapIter pos = newEMap.find(TNewEdge((*itEdges1).v2, (*itEdges2).v2));
						if (pos != newEMap.end()) (*pos).second += weight;
						else newEMap.insert(std::make_pair(TNewEdge((*itEdges1).v2, (*itEdges2).v2), weight));
					} else {
						TEdgeMapIter pos = newEMap.find(TNewEdge((*itEdges2).v2, (*itEdges1).v2));
						if (pos != newEMap.end()) (*pos).second += weight;
						else newEMap.insert(std::make_pair(TNewEdge((*itEdges2).v2, (*itEdges1).v2), weight));
					}
				}
			}
		}
		t2=clock();
		cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
		clear(fullEdges);
	}

	// Insert edges
	cout << "Insert edges..\n";
	t1=clock();
	for(TEdgeMapIter itE = newEMap.begin(); itE != newEMap.end(); ++itE)
		addEdge(g, (*itE).first.first, (*itE).first.second, (*itE).second);
	t2=clock();
	cout<< "done in "<<((t2-t1)/double(CLOCKS_PER_SEC))<<" sec\n";
}

#endif
