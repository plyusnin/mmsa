#ifndef UTIL_H
#define UTIL_H
#include <iostream>
using namespace seqan;

/*
 * Prints values in str as an m x n matrix
 */
template<typename TValue, typename TSpec, typename TSize>
void print(const String<TValue, TSpec>& str, const TSize m, const TSize n){
  for(TSize i=0; i<m; i++){
    for(TSize j=0; j<n; j++){
      std::cout << "\t"<<str[m*j+i];
    }
    std::cout << "\n";
  }
}

#endif

